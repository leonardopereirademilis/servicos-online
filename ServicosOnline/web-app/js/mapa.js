var geocoder;
var map;
var marker;
var latitudeDefault =  -27.5949884;
var longitudeDefault = -48.54817430000003;
var zoomDefault = 16;
var positionDefault;

function initialize(lat,lng,zoom, posDefault) {
	if (posDefault) {
    	if(navigator.geolocation) {
    		navigator.geolocation.getCurrentPosition(function(position) {
    			latitudeDefault = position.coords.latitude;
    			longitudeDefault = position.coords.longitude;    			
    			initialize(latitudeDefault,longitudeDefault,16,false);
    		});
    	}   
    }    	
	
	var latlng = new google.maps.LatLng(lat, lng);
	var options = {
		zoom: zoom,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	
	map = new google.maps.Map(document.getElementById("mapa"), options);
	
	geocoder = new google.maps.Geocoder();
	
	marker = new google.maps.Marker({
		map: map,
		draggable: true,
	});
	
	marker.setPosition(latlng);
	addMapListener();
}

$(document).ready(function () {
	//initialize(latitudeDefault, longitudeDefault, zoomDefault);
	
	function carregarNoMapa(endereco) {
		geocoder.geocode({ 'address': endereco + ', Brasil', 'region': 'BR' }, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					var latitude = results[0].geometry.location.lat();
					var longitude = results[0].geometry.location.lng();
		
					$('#txtEndereco').val(results[0].formatted_address);
					$('#txtLatitude').val(latitude);
                   	$('#txtLongitude').val(longitude);
		
					var location = new google.maps.LatLng(latitude, longitude);
					marker.setPosition(location);
					map.setCenter(location);
					map.setZoom(16);
				}
			}
		})
	}
	
	$("#btnEndereco").click(function() {
		if($(this).val() != "")
			carregarNoMapa($("#txtEndereco").val());
	})
	
	$("#txtEndereco").blur(function() {
		if($(this).val() != "")
			carregarNoMapa($(this).val());
	})

	addMapListener();
	
	$("#txtEndereco").autocomplete({
		source: function (request, response) {
			geocoder.geocode({ 'address': request.term + ', Brasil', 'region': 'BR' }, function (results, status) {
				response($.map(results, function (item) {
					return {
						label: item.formatted_address,
						value: item.formatted_address,
						latitude: item.geometry.location.lat(),
          				longitude: item.geometry.location.lng()
					}
				}));
			})
		},
		select: function (event, ui) {
			$("#txtLatitude").val(ui.item.latitude);
    		$("#txtLongitude").val(ui.item.longitude);
			var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
			marker.setPosition(location);
			map.setCenter(location);
			map.setZoom(16);
		}
	});

});

function addMapListener(){
	google.maps.event.addListener(marker, 'drag', function () {
		geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {  
					//$('#txtEndereco').val(results[0].formatted_address);
					//$('#txtLatitude').val(marker.getPosition().lat());
					//$('#txtLongitude').val(marker.getPosition().lng());
					//$('#numero').val(results[0].address_components[0].short_name);
					//$('#endereco').val(results[0].address_components[1].short_name);
					//$('#bairro').val(results[0].address_components[2].long_name);
					//$('#cep').val(results[0].address_components[7].short_name);
					
					$('#txtNumero').val(results[0].address_components[0].short_name);
					$('#txtLogradouro').val(results[0].address_components[1].short_name);	
					$('#txtBairro').val(results[0].address_components[2].short_name);
					$('#txtCEP').val(results[0].address_components[7].short_name);
					$('#txtEstado').val(results[0].address_components[5].long_name);
					$('#txtUF').val(results[0].address_components[5].short_name);
					$('#txtCidade').val(results[0].address_components[4].long_name);
					$('#txtEndereco').val(results[0].formatted_address);
					$('#txtLatitude').val(marker.getPosition().lat());
					$('#txtLongitude').val(marker.getPosition().lng());
//					$('#txtRetornoCompleto').val(results[0].address_components);
					
					$('#txtNumeroTF').val(results[0].address_components[0].short_name);
					$('#txtLogradouroTF').val(results[0].address_components[1].short_name);	
					$('#txtBairroTF').val(results[0].address_components[2].short_name);
					$('#txtCEPTF').val(results[0].address_components[7].short_name);
					$('#txtEstadoTF').val(results[0].address_components[5].long_name);
					$('#txtUFTF').val(results[0].address_components[5].short_name);
					$('#txtCidadeTF').val(results[0].address_components[4].long_name);
					$('#txtEnderecoTF').val(results[0].formatted_address);
					$('#txtLatitudeTF').val(marker.getPosition().lat());
					$('#txtLongitudeTF').val(marker.getPosition().lng());
//					$('#txtRetornoCompletoTF').val(results[0].address_components);
				}
			}
		});
	});
}