$(document).ready(function(){
//	$("#telefoneComercial").mask("(99) 9999-9999");
//	$("#telefoneResidencial").mask("(99) 9999-9999");
//	$("#celular").mask("(99) 9999-9999");
	$("#cep").mask("99999-999");
	
	$('#telefoneComercial').mask("(99) 9999-9999?9").focusout(function(){
	    var phone, element;
	    element = $(this);
	    element.unmask();
	    phone = element.val().replace(/\D/g, '');
	    if(phone.length > 10) {
	        element.mask("(99) 99999-999?9");
	    } else {
	        element.mask("(99) 9999-9999?9");
	    }
	}).trigger('focusout');
	
	$('#telefoneResidencial').mask("(99) 9999-9999?9").focusout(function(){
	    var phone, element;
	    element = $(this);
	    element.unmask();
	    phone = element.val().replace(/\D/g, '');
	    if(phone.length > 10) {
	        element.mask("(99) 99999-999?9");
	    } else {
	        element.mask("(99) 9999-9999?9");
	    }
	}).trigger('focusout');
	
	$('#celular').mask("(99) 9999-9999?9").focusout(function(){
	    var phone, element;
	    element = $(this);
	    element.unmask();
	    phone = element.val().replace(/\D/g, '');
	    if(phone.length > 10) {
	        element.mask("(99) 99999-999?9");
	    } else {
	        element.mask("(99) 9999-9999?9");
	    }
	}).trigger('focusout');
});