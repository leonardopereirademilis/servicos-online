package br.com.servicosonline

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.social.facebook.api.Facebook
import org.springframework.social.facebook.api.FacebookProfile
import org.springframework.social.facebook.api.impl.FacebookTemplate
import org.springframework.transaction.annotation.Transactional;

import com.the6hours.grails.springsecurity.facebook.FacebookAuthToken


class FacebookAuthService {

    def serviceMethod() {

    }
		
	
//	void onCreate(FacebookUser user, FacebookAuthToken token) {
//		log.info("Creating user: $user for fb user: $token.uid")
//	}
	
//	@Transactional
//	void afterCreate(FacebookUser user, FacebookAuthToken token) {
//		Usuario usuario = Usuario.findByUsername('facebook_' + token.uid)
//		Facebook facebook = new FacebookTemplate(token.accessToken.accessToken)
//		FacebookProfile fbProfile = facebook.userOperations().userProfile
//		String email = fbProfile.email
//		String username = fbProfile.username
//		String firstName = fbProfile.firstName
//		String lastName = fbProfile.lastName
//		usuario.nome = [firstName, lastName].join(' ')
//		usuario.email = email
//		usuario.save()
//	}
	
	/**
	 * Called when we have a new facebook user, called on first login to create all required
	 * data structures. Replaces .createAppUser and .createRoles methods.
	 *
	 * @param token facebook authentication token
	 */
	@Transactional
	FacebookUser create(FacebookAuthToken token) {
		log.info("Create domain for facebook user $token.uid")
		//Use Spring Social Facebook to load details for current user from Facebook API
		Facebook facebook = new FacebookTemplate(token.accessToken.accessToken)
		FacebookProfile fbProfile = facebook.userOperations().userProfile
		String email = fbProfile.email
		String username = fbProfile.email //fbProfile.username
		String firstName = fbProfile.firstName
		String lastName = fbProfile.lastName
		
		Usuario usuario = Usuario.findByEmail(fbProfile.email)
		
		if(!usuario){				
			usuario = new Usuario(
					username: username,
					password: token.accessToken.accessToken, //not really necessary
					enabled: true,
					accountExpired: false,
					accountLocked: false,
					passwordExpired: false,
					//fill with data loaded from Facebook API
					name: [firstName, lastName].join(' '),
					email: email,
					facebookUser: token.uid
					)
			usuario.save(flush:true)
			UsuarioPapel.create(usuario, Papel.findByAuthority('ROLE_USER'))
		}
		
		if(usuario.facebookUser == null){
			usuario.facebookUser = token.uid
			usuario.save(flush:true)
		}
		
		UsuarioPapel.create(usuario, Papel.findByAuthority('ROLE_FACEBOOK'))
		FacebookUser fbUser = new FacebookUser(
				uid: token.uid,
				accessToken: token.accessToken.accessToken,
				accessTokenExpires: token.accessToken.expireAt,
				user: usuario
				)
		fbUser.save()
		return fbUser
	}
}
