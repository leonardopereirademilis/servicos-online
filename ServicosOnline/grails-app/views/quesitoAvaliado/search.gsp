
<%@ page import="br.com.servicosonline.QuesitoAvaliado" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'quesitoAvaliado.label', default: 'Quesito Avaliado')}" />
		<g:set var="entitiesName" value="${message(code: 'quesitosAvaliados.label', default: 'Quesitos Avaliados')}" />
		<title><g:message code="default.search.label" args="[entitiesName]" /></title>
	</head>
	<body>
		<a href="#list-quesitoAvaliado" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entitiesName]" /></g:link></li>
				<sec:ifAnyGranted roles="ROLE_ADMIN">
					<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>	
			</ul>
		</div>
		<div id="list-quesitoAvaliado" class="content scaffold-list" role="main">
			<h1><g:message code="default.search.label" args="[entitiesName]" /></h1>
			
			<br>
			<div class="nav">
				<g:form action="search" method="get">
		            <g:textField name="q" value="${params.q}" required="true"/>
		            <g:submitButton name="search" value="Buscar"/>
		        </g:form>
		    </div>
			<br>
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<g:set var="haveQuery" value="${params.q?.trim()}" />
			<g:set var="haveResults" value="${searchResult?.results}" />
	
			<g:if test="${haveResults}">
				<div class="list">
					<table>
						<thead>
							<tr>
							
								<th><g:message code="quesitoAvaliado.avaliacao.label" default="Avaliação" /></th>
							
								<th><g:message code="quesitoAvaliado.quesito.label" default="Quesito" /></th>
							
								<g:sortableColumn property="valor" title="${message(code: 'quesitoAvaliado.valor.label', default: 'Valor')}" />
							
							</tr>
						</thead>
						<tbody>
						<g:set var="totalPages"	value="${Math.ceil(searchResult.total / searchResult.max)}" />
						<g:each in="${searchResult.results}" status="i" var="quesitoAvaliadoInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							
								<td><g:link action="show" id="${quesitoAvaliadoInstance.id}">${fieldValue(bean: quesitoAvaliadoInstance, field: "avaliacao.prestadorServico")}/${fieldValue(bean: quesitoAvaliadoInstance, field: "avaliacao.usuario")}</g:link></td>
							
								<td>${fieldValue(bean: quesitoAvaliadoInstance, field: "quesito.nome")}</td>
							
								<td><div class="rating_div"><input value="${fieldValue(bean: quesitoAvaliadoInstance, field: "valor")}" type="number" class="rating" min=0 max=5 step=1 data-size="xs" disabled="disabled" ></div></td>
							
							</tr>
						</g:each>
						</tbody>
					</table>
				</div>
			</g:if>		
			<g:else>
				<div class="no_results">
					<g:message code="search.no.result.label" default="Nenhum resultado encontrado..."/>
				</div>
			</g:else>
			<div class="pagination">
				<g:paginate params="[q: params.q]" total="${searchResult.total}" />
			</div>
		</div>
	</body>
</html>
