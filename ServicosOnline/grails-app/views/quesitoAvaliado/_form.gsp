<%@ page import="br.com.servicosonline.QuesitoAvaliado" %>



<div class="fieldcontain ${hasErrors(bean: quesitoAvaliadoInstance, field: 'avaliacao', 'error')} required">
	<label for="avaliacao">
		<g:message code="quesitoAvaliado.avaliacao.label" default="Avaliação" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="avaliacao" name="avaliacao.id" from="${br.com.servicosonline.Avaliacao.list()}" optionKey="id" required="" value="${quesitoAvaliadoInstance?.avaliacao?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quesitoAvaliadoInstance, field: 'quesito', 'error')} required">
	<label for="quesito">
		<g:message code="quesitoAvaliado.quesito.label" default="Quesito" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="quesito" name="quesito.id" from="${br.com.servicosonline.Quesito.list()}" optionKey="id" required="" value="${quesitoAvaliadoInstance?.quesito?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quesitoAvaliadoInstance, field: 'valor', 'error')} required">
	<label for="valor">
		<g:message code="quesitoAvaliado.valor.label" default="Valor" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="valor" type="number" value="${quesitoAvaliadoInstance.valor}" required=""/>
</div>

