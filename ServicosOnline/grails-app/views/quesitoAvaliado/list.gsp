
<%@ page import="br.com.servicosonline.QuesitoAvaliado" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'quesitoAvaliado.label', default: 'Quesito Avaliado')}" />
		<g:set var="entitiesName" value="${message(code: 'quesitosAvaliados.label', default: 'Quesitos Avaliados')}" />
		<title><g:message code="default.list.label" args="[entitiesName]" /></title>
	</head>
	<body>
		<a href="#list-quesitoAvaliado" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<sec:ifAnyGranted roles="ROLE_ADMIN">
					<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>	
			</ul>
		</div>
		<div id="list-quesitoAvaliado" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entitiesName]" /></h1>
			
			<br>
			<div class="nav">
				<g:form action="search" method="get">
		            <g:textField name="q" value="${params.q}" required="true"/>
		            <g:submitButton name="search" value="Buscar"/>
		        </g:form>
		    </div>
			<br>
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="quesitoAvaliado.avaliacao.label" default="Avaliação" /></th>
					
						<th><g:message code="quesitoAvaliado.quesito.label" default="Quesito" /></th>
					
						<g:sortableColumn property="valor" title="${message(code: 'quesitoAvaliado.valor.label', default: 'Valor')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${quesitoAvaliadoInstanceList}" status="i" var="quesitoAvaliadoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td style="text-align: center;"><g:link action="show" id="${quesitoAvaliadoInstance.id}">${fieldValue(bean: quesitoAvaliadoInstance, field: "avaliacao.prestadorServico")}/${fieldValue(bean: quesitoAvaliadoInstance, field: "avaliacao.usuario")}</g:link></td>
					
						<td style="text-align: center;">${fieldValue(bean: quesitoAvaliadoInstance, field: "quesito.nome")}</td>
					
						<td style="text-align: center;"><div class="rating_div"><input value="${fieldValue(bean: quesitoAvaliadoInstance, field: "valor")}" type="number" class="rating" min=0 max=5 step=1 data-size="xs" disabled="disabled" ></div></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${quesitoAvaliadoInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
