<%@ page import="br.com.servicosonline.QuesitoAvaliado" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'quesitoAvaliado.label', default: 'Quesito Avaliado')}" />
		<g:set var="entitiesName" value="${message(code: 'quesitosAvaliados.label', default: 'Quesitos Avaliados')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#create-quesitoAvaliado" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entitiesName]" /></g:link></li>
			</ul>
		</div>
		<div id="create-quesitoAvaliado" class="content scaffold-create" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${quesitoAvaliadoInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${quesitoAvaliadoInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form action="save" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
