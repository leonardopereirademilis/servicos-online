
<%@ page import="br.com.servicosonline.QuesitoAvaliado" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'quesitoAvaliado.label', default: 'Quesito Avaliado')}" />
		<g:set var="entitiesName" value="${message(code: 'quesitosAvaliados.label', default: 'Quesitos Avaliados')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-quesitoAvaliado" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entitiesName]" /></g:link></li>
				<sec:ifAnyGranted roles="ROLE_ADMIN">
					<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>	
			</ul>
		</div>
		<div id="show-quesitoAvaliado" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list quesitoAvaliado">
			
				<g:if test="${quesitoAvaliadoInstance?.avaliacao}">
				<li class="fieldcontain">
					<span id="avaliacao-label" class="property-label"><g:message code="quesitoAvaliado.avaliacao.label" default="Avaliação" /></span>
					
						<span class="property-value" aria-labelledby="avaliacao-label"><g:link controller="avaliacao" action="show" id="${quesitoAvaliadoInstance?.avaliacao?.id}">${quesitoAvaliadoInstance?.avaliacao?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${quesitoAvaliadoInstance?.quesito}">
				<li class="fieldcontain">
					<span id="quesito-label" class="property-label"><g:message code="quesitoAvaliado.quesito.label" default="Quesito" /></span>
					
						<span class="property-value" aria-labelledby="quesito-label"><g:link controller="quesito" action="show" id="${quesitoAvaliadoInstance?.quesito?.id}">${quesitoAvaliadoInstance?.quesito?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${quesitoAvaliadoInstance?.valor}">
				<li class="fieldcontain">
					<span id="valor-label" class="property-label"><g:message code="quesitoAvaliado.valor.label" default="Valor" /></span>
					
						<span class="property-value" aria-labelledby="valor-label"><g:fieldValue bean="${quesitoAvaliadoInstance}" field="valor"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<sec:ifAnyGranted roles="ROLE_ADMIN">
				<g:form>
					<fieldset class="buttons">
						<g:hiddenField name="id" value="${quesitoAvaliadoInstance?.id}" />
						<g:link class="edit" action="edit" id="${quesitoAvaliadoInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
						<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
					</fieldset>
				</g:form>
			</sec:ifAnyGranted>	
		</div>
	</body>
</html>
