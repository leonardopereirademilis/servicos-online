<%@ page import="br.com.servicosonline.CategoriaServico" %>



<div class="fieldcontain ${hasErrors(bean: categoriaServicoInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="categoriaServico.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" value="${categoriaServicoInstance?.nome}" required="true"/>
</div>

<div class="fieldcontain ${hasErrors(bean: categoriaServicoInstance, field: 'quesitos', 'error')} required">
	<label for="quesitos">
		<g:message code="categoriaServico.quesitos.label" default="Quesitos" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="quesitos" from="${br.com.servicosonline.Quesito.list()}" multiple="multiple" optionKey="id" size="5" value="${categoriaServicoInstance?.quesitos*.id}" class="many-to-many" optionValue="nome" required="true"/>
</div>

<g:if test="${categoriaServicoInstance?.icone}">
	<div class="fieldcontain">
		<span id="image-label" class="property-label"><g:message code="categoriaServico.icone.label" default="" /></span>
		<span class="property-value" aria-labelledby="image-label"><img class="icone_small" src="${createLink(action:'icone_image', id:categoriaServicoInstance?.id)}" /></span>					
	</div>
	
	<div class="fieldcontain ${hasErrors(bean: categoriaServicoInstance, field: 'icone', 'error')}">
		<label for="avatar">
			<g:message code="categoriaServico.icone.label" default="Ícone (4MB)" />

		</label>
		<input type="file" name="icone" id="icone" />
	</div>
</g:if>
<g:else>
	<div class="fieldcontain ${hasErrors(bean: categoriaServicoInstance, field: 'icone', 'error')} required">
		<label for="avatar">
			<g:message code="categoriaServico.icone.label" default="Ícone (4MB)" />
			<span class="required-indicator">*</span>
		</label>
		<input type="file" name="icone" id="icone" required="required" />
	</div>
</g:else>
