
<%@ page import="br.com.servicosonline.CategoriaServico" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'categoriaServico.label', default: 'Categoria de Serviço')}" />
		<g:set var="entitiesName" value="${message(code: 'categoriasServico.label', default: 'Categorias de Serviço')}" />
		<title><g:message code="default.search.label" args="[entitiesName]" /></title>
	</head>
	<body>
		<a href="#list-categoriaServico" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entitiesName]" /></g:link></li>
				<sec:ifAnyGranted roles="ROLE_ADMIN">
					<li><g:link class="create" action="create"><g:message code="default.nova.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>	
			</ul>
		</div>
		<div id="list-categoriaServico" class="content scaffold-list" role="main">
			<h1><g:message code="default.search.label" args="[entitiesName]" /></h1>
			
			<br>
			<div class="nav">
				<g:form action="search" method="get">
		            <g:textField name="q" value="${params.q}" required="true"/>
		            <g:submitButton name="search" value="Buscar"/>
		        </g:form>
		    </div>
			<br>
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<g:set var="haveQuery" value="${params.q?.trim()}" />
			<g:set var="haveResults" value="${searchResult?.results}" />
	
			<g:if test="${haveResults}">
				<div class="list">
					<table>
						<thead>
							<tr>
								
								<th><g:message code="categoriaServico.icone.label" default="Ícone" /></th>
							
								<g:sortableColumn property="nome" title="${message(code: 'categoriaServico.nome.label', default: 'Nome')}" />
							
							</tr>
						</thead>
						<tbody>
						<g:set var="totalPages"	value="${Math.ceil(searchResult.total / searchResult.max)}" />
						<g:each in="${searchResult.results}" status="i" var="categoriaServicoInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							
								<td style="text-align: center;">
									<span class="property-value" aria-labelledby="image-label"><img class="icone_small" src="${createLink(controller:'categoriaServico', action:'icone_image', id:categoriaServicoInstance?.id)}" /></span>					
								</td>
							
								<td><g:link action="show" id="${categoriaServicoInstance.id}">${fieldValue(bean: categoriaServicoInstance, field: "nome")}</g:link></td>
							
							</tr>
						</g:each>
						</tbody>
					</table>
				</div>	
			</g:if>
			<g:else>
				<div class="no_results">
					<g:message code="search.no.result.label" default="Nenhum resultado encontrado..."/>
				</div>
			</g:else>
			<div class="pagination">
				<g:paginate params="[q: params.q]" total="${searchResult.total}" />
			</div>
		</div>
	</body>
</html>
