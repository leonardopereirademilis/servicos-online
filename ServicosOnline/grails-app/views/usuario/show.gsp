
<%@page import="org.springframework.security.core.authority.GrantedAuthorityImpl"%>
<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@ page import="br.com.servicosonline.Usuario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuário')}" />
		<g:set var="entitiesName" value="${message(code: 'usuarios.label', default: 'Usuários')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-usuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entitiesName]" /></g:link></li>
				<sec:ifAnyGranted roles="ROLE_ADMIN">
					<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>	
			</ul>
		</div>
		<div id="show-usuario" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list usuario">
			
<%--			${session.SPRING_SECURITY_CONTEXT?.authentication?.uid?.toString()}				--%>
<%--				${session}--%>
<%--				<br><br>--%>
<%--				${session.id}--%>
<%--				<br><br>--%>
<%--				${session.getAttributeNames()}--%>
<%--				<br><br>
				${session.getValueNames()}
				<br><br>				
																								
<%--				<img src="https://graph.facebook.com/${usuarioInstance.facebookUser}/picture?type=large" />Large size photo --%>

<%--				<img src="https://graph.facebook.com/${usuarioInstance.facebookUser}/picture?type=normal" />Medium size photo --%>

<%--				<img src="https://graph.facebook.com/${usuarioInstance.facebookUser}/picture?type=small" />Small size photo --%>

<%--				<img src="https://graph.facebook.com/${usuarioInstance.facebookUser}/picture?type=square" />Square photo --%>
				
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="usuario.image.label" default="" /></span>
					<g:if test="${session.SPRING_SECURITY_CONTEXT?.authentication.hasProperty('uid')}">
						<span class="property-value" aria-labelledby="image-label"><div class="avatar_div"><img class="avatar" src="https://graph.facebook.com/${usuarioInstance.facebookUser}/picture?type=normal" /></div></span>
					</g:if>
					<g:else>
						<g:if test="${usuarioInstance?.avatar}">
							<span class="property-value" aria-labelledby="image-label"><div class="avatar_div"><img class="avatar" src="${createLink(action:'avatar_image', id:usuarioInstance?.id)}" /></div></span>					
	  					</g:if>
	  					<g:else>
	  						<span class="property-value" aria-labelledby="image-label"><div class="avatar_div"><img class="avatar default" src="/ServicosOnline/images/user-default.png" /></div></span>
	  					</g:else>
	  				</g:else>	
  				</li>	
				
			
				<g:if test="${usuarioInstance?.nome}">
				<li class="fieldcontain">
					<span id="nome-label" class="property-label"><g:message code="usuario.nome.label" default="Nome" /></span>
					
						<span class="property-value bolder" aria-labelledby="nome-label"><g:fieldValue bean="${usuarioInstance}" field="nome"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioInstance?.email}">
				<li class="fieldcontain">
					<span id="email-label" class="property-label"><g:message code="usuario.email.label" default="E-mail" /></span>
					
						<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${usuarioInstance}" field="email"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioInstance?.password}">
				<li class="fieldcontain">
					<span id="password-label" class="property-label"><g:message code="usuario.password.label" default="Password" /></span>
					
						<span class="property-value" aria-labelledby="password-label"><g:fieldValue bean="${usuarioInstance}" field="password"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${usuarioInstance?.endereco}">
				<li class="fieldcontain">
					<span id="endereco-label" class="property-label"><g:message code="usuario.endereco.label" default="Endereço" /></span>
					
						<span class="property-value" aria-labelledby="endereco-label"><g:fieldValue bean="${usuarioInstance}" field="endereco"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${usuarioInstance?.numero}">
				<li class="fieldcontain">
					<span id="numero-label" class="property-label"><g:message code="usuario.numero.label" default="Número" /></span>
					
						<span class="property-value" aria-labelledby="numero-label"><g:fieldValue bean="${usuarioInstance}" field="numero"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${usuarioInstance?.complemento}">
				<li class="fieldcontain">
					<span id="complemento-label" class="property-label"><g:message code="usuario.complemento.label" default="Complemento" /></span>
					
						<span class="property-value" aria-labelledby="complemento-label"><g:fieldValue bean="${usuarioInstance}" field="complemento"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${usuarioInstance?.bairro}">
				<li class="fieldcontain">
					<span id="bairro-label" class="property-label"><g:message code="usuario.bairro.label" default="Bairro" /></span>
					
						<span class="property-value" aria-labelledby="bairro-label"><g:fieldValue bean="${usuarioInstance}" field="bairro"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${usuarioInstance?.cep}">
				<li class="fieldcontain">
					<span id="cep-label" class="property-label"><g:message code="usuario.cep.label" default="CEP" /></span>
					
						<span class="property-value" aria-labelledby="cep-label"><g:fieldValue bean="${usuarioInstance}" field="cep"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${usuarioInstance?.estado}">
				<li class="fieldcontain">
					<span id="estado-label" class="property-label"><g:message code="usuario.estado.label" default="Estado" /></span>
					
						<span class="property-value" aria-labelledby="estado-label"><g:link controller="estado" action="show" id="${usuarioInstance?.estado?.id}">${usuarioInstance?.estado?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
										
				<g:if test="${usuarioInstance?.cidade}">
				<li class="fieldcontain">
					<span id="cidade-label" class="property-label"><g:message code="usuario.cidade.label" default="Cidade" /></span>
					
						<span class="property-value" aria-labelledby="cidade-label"><g:link controller="cidade" action="show" id="${usuarioInstance?.cidade?.id}">${usuarioInstance?.cidade?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
															
				<g:if test="${usuarioInstance?.telefoneResidencial}">
				<li class="fieldcontain">
					<span id="telefoneResidencial-label" class="property-label"><g:message code="usuario.telefoneResidencial.label" default="Telefone Residencial" /></span>
					
						<span class="property-value" aria-labelledby="telefoneResidencial-label"><g:fieldValue bean="${usuarioInstance}" field="telefoneResidencial"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${usuarioInstance?.celular}">
				<li class="fieldcontain">
					<span id="celular-label" class="property-label"><g:message code="usuario.celular.label" default="Celular" /></span>
					
						<span class="property-value" aria-labelledby="celular-label"><g:fieldValue bean="${usuarioInstance}" field="celular"/></span>
					
				</li>
				</g:if>								
			
				<g:if test="${usuarioInstance?.telefoneComercial}">
				<li class="fieldcontain">
					<span id="telefoneComercial-label" class="property-label"><g:message code="usuario.telefoneComercial.label" default="Telefone Comercial" /></span>
					
						<span class="property-value" aria-labelledby="telefoneComercial-label"><g:fieldValue bean="${usuarioInstance}" field="telefoneComercial"/></span>
					
				</li>
				</g:if>
										
				<g:if test="${usuarioInstance?.usuarios}">
				<li class="fieldcontain">
					<span id="usuarios-label" class="property-label"><g:message code="usuario.usuarios.label" default="Usuários" /></span>
					
						<g:each in="${usuarioInstance.usuarios}" var="u">
						<span class="property-value" aria-labelledby="usuarios-label"><g:link controller="usuario" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			
			<g:if test="${(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id == usuarioInstance?.id) || SecurityContextHolder.getContext().getAuthentication().authorities.contains(new GrantedAuthorityImpl('ROLE_ADMIN'))}">
				<g:form>
					<fieldset class="buttons">
						<g:hiddenField name="id" value="${usuarioInstance?.id}" />
						<g:link class="edit" action="edit" id="${usuarioInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
						<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
					</fieldset>
				</g:form>
			</g:if>
		</div>
	</body>
</html>
