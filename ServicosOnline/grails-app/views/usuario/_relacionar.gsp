<%@page import="br.com.servicosonline.UsuarioController"%>

<g:if test="${usuarioInstance.id != session.SPRING_SECURITY_CONTEXT?.authentication?.principal?.getId()}">
	<g:set var="relacionamento" value="${usuarioInstance.verificarRelacionamento()}" />
	<g:if test="${relacionamento == 0}">
		<div class="user-add-btn" onclick="${remoteFunction(controller:'usuario', action: 'relacionarUsuario', id: usuarioInstance.id, update:'relacionar_' + usuarioInstance.id)}" title="Solicitar amizade"></div>
	</g:if>
	<g:elseif test="${relacionamento == 1}">
		<div class="user-wait-btn" title="Aguardando resposta de solicitação de amizade"></div>
	</g:elseif>
	<g:elseif test="${relacionamento == 2}">
		<div class="user-confirm-btn" onclick="${remoteFunction(controller:'usuario', action: 'relacionarUsuario', id: usuarioInstance.id, update:'relacionar_' + usuarioInstance.id)}" title="Aceitar solicitação de amizade"></div><div class="user-remove-btn" onclick="${remoteFunction(action: 'naoRelacionarUsuario', id: usuarioInstance.id, update:'relacionar_' + usuarioInstance.id)}" title="Negar solicitação de amizade"></div>
	</g:elseif>
	<g:elseif test="${relacionamento == 3}">
		<div class="user-btn" title="Vocês já são amigos"></div>
	</g:elseif>
	<g:elseif test="${relacionamento == 4}">
		<div class="user-confirm-btn" onclick="${remoteFunction(controller:'usuario', action: 'relacionarUsuario', id: usuarioInstance.id, update:'relacionar_' + usuarioInstance.id)}" title="Aceitar solicitação de amizade"></div><div class="user-remove-btn-disabled"></div>
	</g:elseif>
</g:if>