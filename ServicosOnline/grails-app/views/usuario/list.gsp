
<%@ page import="br.com.servicosonline.Usuario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuário')}" />
		<g:set var="entitiesName" value="${message(code: 'usuarios.label', default: 'Usuários')}" />
		<title><g:message code="default.list.label" args="[entitiesName]" /></title>
	</head>
	<body>
		<a href="#list-usuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<sec:ifAnyGranted roles="ROLE_ADMIN">
					<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>	
			</ul>
		</div>
		<div id="list-usuario" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entitiesName]" /></h1>
			
			<br>
			<div class="nav">
				<g:form action="search" method="get">
		            <g:textField name="q" value="${params.q}" required="true"/>
		            <g:submitButton name="search" value="Buscar"/>
		        </g:form>
		    </div>
			<br>
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="nome" title="${message(code: 'usuario.nome.label', default: 'Nome')}" />
						<g:sortableColumn property="email" title="${message(code: 'usuario.email.label', default: 'E-mail')}" />
						<g:sortableColumn property="cidade" title="${message(code: 'usuario.cidade.label', default: 'Cidade')}" />
						<g:sortableColumn property="estado" title="${message(code: 'usuario.email.estado', default: 'Estado')}" />
<%--						<g:sortableColumn property="telefoneResidencial" title="${message(code: 'usuario.telefone.residencial.label', default: 'Telefone Residencial')}" />--%>
<%--						<g:sortableColumn property="telefoneComercial" title="${message(code: 'usuario.telefone.comercial.label', default: 'Telefone Comercial')}" />--%>
<%--						<g:sortableColumn property="celular" title="${message(code: 'usuario.celular.label', default: 'Celular')}" />--%>
						<th><g:message code="usuario.acao.label" default="Ação" /></th>
																
					</tr>
				</thead>
				<tbody>
				<g:each in="${usuarioInstanceList}" status="i" var="usuarioInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>
							<g:link action="show" id="${usuarioInstance.id}">
								<g:if test="${usuarioInstance?.facebookUser}">
									<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small" src="https://graph.facebook.com/${usuarioInstance?.facebookUser}/picture?type=normal" /></div></span>
								</g:if>
								<g:else>
									<g:if test="${usuarioInstance?.avatar}">
										<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small" src="${createLink(controller:'usuario', action:'avatar_image', id:usuarioInstance?.id)}" /></div></span>					
				  					</g:if>
				  					<g:else>
				  						<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small default" src="/ServicosOnline/images/user-default.png" /></div></span>
				  					</g:else>
				  				</g:else>	
								${fieldValue(bean: usuarioInstance, field: "nome")}
							</g:link>
						</td>
					
						<td>${fieldValue(bean: usuarioInstance, field: "email")}</td>
					
						<td style="text-align: center;">${fieldValue(bean: usuarioInstance, field: "cidade")}</td>
					
						<td style="text-align: center;">${fieldValue(bean: usuarioInstance, field: "estado.uf")}</td>
						
						<td style="text-align: center;">
							<div id="relacionar_${usuarioInstance.id}">
								<g:render template="/usuario/relacionar" model="[usuarioInstance: usuarioInstance]" ></g:render>
							</div>
						</td>
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${usuarioInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
