<%@ page import="br.com.servicosonline.Usuario" %>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'avatar', 'error')}">
	<label for="avatar">
		<g:message code="usuario.avatar.label" default="Avatar (4MB)" />
		
	</label>
	<input type="file" name="avatar" id="avatar" />
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="usuario.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" value="${usuarioInstance?.nome}" required="true"/>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="usuario.email.label" default="E-mail" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="email" value="${usuarioInstance?.email}" required="true"/>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="usuario.password.label" default="Password"/>
		<span class="required-indicator">*</span>
	</label>
	<g:field type="password" name="password" value="${usuarioInstance?.password}" required="true"/>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'endereco', 'error')} ">
	<label for="endereco">
		<g:message code="usuario.endereco.label" default="Endereço" />
		
	</label>
	<g:textField name="endereco" value="${usuarioInstance?.endereco}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'numero', 'error')} ">
	<label for="numero">
		<g:message code="usuario.numero.label" default="Número" />
		
	</label>
	<g:field name="numero" type="number" value="${usuarioInstance?.numero}" />
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'complemento', 'error')} ">
	<label for="complemento">
		<g:message code="usuario.complemento.label" default="Complemento" />
		
	</label>
	<g:textField name="complemento" value="${usuarioInstance?.complemento}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'bairro', 'error')} ">
	<label for="bairro">
		<g:message code="usuario.bairro.label" default="Bairro" />
		
	</label>
	<g:textField name="bairro" value="${usuarioInstance?.bairro}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'cep', 'error')} ">
	<label for="cep">
		<g:message code="usuario.cep.label" default="CEP" />
		
	</label>
	<g:textField name="cep" value="${usuarioInstance?.cep}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'estado', 'error')} required">
	<label for="estado">
		<g:message code="usuario.estado.label" default="Estado" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="estado" name="estado.id" from="${br.com.servicosonline.Estado.list()}" optionKey="id" required="" value="${usuarioInstance?.estado?.id}" class="many-to-one"  noSelection="['':'Selecione um estado...']" onchange="${remoteFunction(controller: 'cidade', action: 'buscaCidades', params: '\'estado=\' + this.value', update:'cidadeSelect')}" optionValue="nome"/>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'cidade', 'error')} required">
	<label for="cidade">
		<g:message code="usuario.cidade.label" default="Cidade" />
		<span class="required-indicator">*</span>
	</label>
<%--	<g:select id="cidade" name="cidade.id" from="${br.com.servicosonline.Cidade.list()}" optionKey="id" required="" value="${usuarioInstance?.cidade?.id}" class="many-to-one" noSelection="['':'Selecione um estado antes...']" disabled="true"/>--%>
		<div id="cidadeSelect" style="display: inline;">
			<g:render template="/cidade/cidade" model="[cidades: cidades, cidade:usuarioInstance?.cidade?.id]" ></g:render>
		</div>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'telefoneResidencial', 'error')} ">
	<label for="telefoneResidencial">
		<g:message code="usuario.telefoneResidencial.label" default="Telefone Residencial" />
		
	</label>
	<g:textField name="telefoneResidencial" value="${usuarioInstance?.telefoneResidencial}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'celular', 'error')} ">
	<label for="celular">
		<g:message code="usuario.celular.label" default="Celular" />
		
	</label>
	<g:textField name="celular" value="${usuarioInstance?.celular}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'telefoneComercial', 'error')} ">
	<label for="telefoneComercial">
		<g:message code="usuario.telefoneComercial.label" default="Telefone Comercial" />
		
	</label>
	<g:textField name="telefoneComercial" value="${usuarioInstance?.telefoneComercial}"/>
</div>