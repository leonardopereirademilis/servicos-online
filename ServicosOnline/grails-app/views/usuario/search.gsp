
<%@ page import="br.com.servicosonline.Usuario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuário')}" />
		<g:set var="entitiesName" value="${message(code: 'usuarios.label', default: 'Usuários')}" />
		<title><g:message code="default.search.label" args="[entitiesName]" /></title>
	</head>
	<body>
		<a href="#list-usuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entitiesName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-usuario" class="content scaffold-list" role="main">
			<h1><g:message code="default.search.label" args="[entitiesName]" /></h1>
			
			<br>
			<div class="nav">
				<g:form action="search" method="get">
		            <g:textField name="q" value="${params.q}" required="true"/>
		            <g:submitButton name="search" value="Buscar"/>
		        </g:form>
		    </div>
			<br>
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<g:set var="haveQuery" value="${params.q?.trim()}" />
			<g:set var="haveResults" value="${searchResult?.results}" />
	
			<g:if test="${haveResults}">
				<div class="list">
					<table>
						<thead>
							<tr>
							
								<g:sortableColumn property="nome" title="${message(code: 'usuario.nome.label', default: 'Nome')}" />
								<g:sortableColumn property="email" title="${message(code: 'usuario.email.label', default: 'E-mail')}" />
								<g:sortableColumn property="cidade" title="${message(code: 'usuario.cidade.label', default: 'Cidade')}" />
								<g:sortableColumn property="estado" title="${message(code: 'usuario.email.estado', default: 'Estado')}" />
								<th><g:message code="usuario.acao.label" default="Ação" /></th>										
							</tr>
						</thead>
						<tbody>
						<g:set var="totalPages"	value="${Math.ceil(searchResult.total / searchResult.max)}" />
						<g:each in="${searchResult.results}" status="i" var="usuarioInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							
								<td>
									<g:link action="show" id="${usuarioInstance.id}">
										<g:if test="${usuarioInstance?.facebookUser}">
											<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small" src="https://graph.facebook.com/${usuarioInstance?.facebookUser}/picture?type=normal" /></div></span>
										</g:if>
										<g:else>
											<g:if test="${usuarioInstance?.avatar}">
												<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small" src="${createLink(controller:'usuario', action:'avatar_image', id:usuarioInstance?.id)}" /></div></span>					
						  					</g:if>
						  					<g:else>
						  						<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small default" src="/ServicosOnline/images/user-default.png" /></div></span>
						  					</g:else>
						  				</g:else>	
										${fieldValue(bean: usuarioInstance, field: "nome")}
									</g:link>
								</td>
							
								<td>${fieldValue(bean: usuarioInstance, field: "email")}</td>
							
								<td style="text-align: center;">${fieldValue(bean: usuarioInstance, field: "cidade")}</td>
							
								<td style="text-align: center;">${fieldValue(bean: usuarioInstance, field: "estado.uf")}</td>
								
								<td style="text-align: center;">
									<div id="relacionar_${usuarioInstance.id}">
										<g:if test="${usuarioInstance.id == session.SPRING_SECURITY_CONTEXT?.authentication?.principal?.getId()}">
											-
										</g:if>
										<g:else>
											<g:render template="/usuario/relacionar" model="[usuarioInstance: usuarioInstance]" ></g:render>
										</g:else>
									</div>
								</td>
							
							</tr>
						</g:each>
						</tbody>
					</table>
				</div>
			</g:if>	
			<g:else>
				<div class="no_results">
					<g:message code="search.no.result.label" default="Nenhum resultado encontrado..."/>
				</div>
			</g:else>
			<div class="pagination">
				<g:paginate params="[q: params.q]" total="${searchResult.total}" />
			</div>
			
		</div>
	</body>
</html>
