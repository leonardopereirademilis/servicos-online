
<%@ page import="br.com.servicosonline.PrestadorServico" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'prestadorServico.label', default: 'Prestador de Serviço')}" />
		<g:set var="entitiesName" value="${message(code: 'prestadoresServico.label', default: 'Prestadores de Serviço')}" />
		<g:set var="entityAvaliacaoName" value="${message(code: 'avaliacao.label', default: 'Avaliação')}" />
		<g:set var="entitiesAvaliacaoName" value="${message(code: 'avaliacao.label', default: 'Avaliações')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    	<meta charset="utf-8">
    	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    	<script>
			var map;
			function initialize() {
				var myLatLng = new google.maps.LatLng(${prestadorServicoInstance?.txtLatitude}, ${prestadorServicoInstance?.txtLongitude});
  				var mapOptions = {
    				zoom: 16,
    				center: myLatLng
  				};
  				map = new google.maps.Map(document.getElementById('map-canvas'),
      				mapOptions);

  				var image = 'http://localhost:8080/ServicosOnline/static/images/push_pin_pink.png';
  			  	var positionMarker = new google.maps.Marker({
  			    	position: myLatLng,
  			      	map: map,
  			      	icon: image,
  			      	title: '${prestadorServicoInstance?.nome}'
  			  	});  			  				
			}

			google.maps.event.addDomListener(window, 'load', initialize);

    	</script>		
	</head>
	<body>
		<a href="#show-prestadorServico" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entitiesName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-prestadorServico" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list prestadorServico">
			
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="prestadorServico.image.label" default="" /></span>
					<g:if test="${prestadorServicoInstance?.avatar}">
						<span class="property-value" aria-labelledby="image-label"><div class="avatar_div"><img class="avatar" src="${createLink(action:'avatar_image', id:prestadorServicoInstance?.id)}" /></div></span>					
	  				</g:if>
	  				<g:else>
	  					<span class="property-value" aria-labelledby="image-label"><div class="avatar_div"><img class="avatar default" src="/ServicosOnline/images/prestadorServico-default.png" /></div></span>
	  				</g:else>
  				</li>	
			
				<g:if test="${prestadorServicoInstance?.nome}">
				<li class="fieldcontain">
					<span id="nome-label" class="property-label"><g:message code="prestadorServico.nome.label" default="Nome" /></span>
					
						<span class="property-value nome" aria-labelledby="nome-label"><g:fieldValue bean="${prestadorServicoInstance}" field="nome"/><div id="favorito"><g:render template="/prestadorServico/favorito" model="[prestadorServicoInstance: prestadorServicoInstance]" ></g:render></div></span>
					
				</li>
				</g:if>
				
				<li class="fieldcontain">
					<span id="avaliacao-label" class="property-label"><g:message code="prestadorServico.avaliacao.label" default="Avaliação" /></span>
					<span class="property-value" aria-labelledby="nome-label">
						<div class="rating_div">
							<input value="${prestadorServicoInstance.getMediaAvaliacoes()}" type="number" class="rating" min=0 max=5 step=1 data-size="xs" disabled="disabled" >
						</div>
					</span>	
				</li>
				
				
				<g:if test="${prestadorServicoInstance?.tipoPrestadorServico}">
				<li class="fieldcontain">
					<span id="tipoPrestadorServico-label" class="property-label"><g:message code="prestadorServico.tipoPrestadorServico.label" default="Tipo Prestador Serviço" /></span>
					
						<span class="property-value" aria-labelledby="tipoPrestadorServico-label"><g:link controller="tipoPrestadorServico" action="show" id="${prestadorServicoInstance?.tipoPrestadorServico?.id}">${prestadorServicoInstance?.tipoPrestadorServico?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
									
				<g:if test="${prestadorServicoInstance?.categoriasServico}">
				<li class="fieldcontain">
					<span id="categoriasServico-label" class="property-label"><g:message code="prestadorServico.categoriasServico.label" default="Categorias Serviço" /></span>
					
						<g:each in="${prestadorServicoInstance.categoriasServico}" var="c">
						<span class="property-value" aria-labelledby="categoriasServico-label"><g:link controller="categoriaServico" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
				
				<g:if test="${prestadorServicoInstance?.telefoneResidencial}">
				<li class="fieldcontain">
					<span id="telefoneResidencial-label" class="property-label"><g:message code="prestadorServico.telefoneResidencial.label" default="Telefone Residencial" /></span>
					
						<span class="property-value" aria-labelledby="telefoneResidencial-label"><g:fieldValue bean="${prestadorServicoInstance}" field="telefoneResidencial"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${prestadorServicoInstance?.celular}">
				<li class="fieldcontain">
					<span id="celular-label" class="property-label"><g:message code="prestadorServico.celular.label" default="Celular" /></span>
					
						<span class="property-value" aria-labelledby="celular-label"><g:fieldValue bean="${prestadorServicoInstance}" field="celular"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${prestadorServicoInstance?.telefoneComercial}">
				<li class="fieldcontain">
					<span id="telefoneComercial-label" class="property-label"><g:message code="prestadorServico.telefoneComercial.label" default="Telefone Comercial" /></span>
					
						<span class="property-value" aria-labelledby="telefoneComercial-label"><g:fieldValue bean="${prestadorServicoInstance}" field="telefoneComercial"/></span>
					
				</li>
				</g:if>
																									
<%--				<g:if test="${prestadorServicoInstance?.endereco}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="endereco-label" class="property-label"><g:message code="prestadorServico.endereco.label" default="Endereço" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="endereco-label"><g:fieldValue bean="${prestadorServicoInstance}" field="endereco"/></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--				--%>
<%--				<g:if test="${prestadorServicoInstance?.numero}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="numero-label" class="property-label"><g:message code="prestadorServico.numero.label" default="Número" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="numero-label"><g:fieldValue bean="${prestadorServicoInstance}" field="numero"/></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--				--%>
<%--				<g:if test="${prestadorServicoInstance?.complemento}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="complemento-label" class="property-label"><g:message code="prestadorServico.complemento.label" default="Complemento" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="complemento-label"><g:fieldValue bean="${prestadorServicoInstance}" field="complemento"/></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--				--%>
<%--				<g:if test="${prestadorServicoInstance?.bairro}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="bairro-label" class="property-label"><g:message code="prestadorServico.bairro.label" default="Bairro" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="bairro-label"><g:fieldValue bean="${prestadorServicoInstance}" field="bairro"/></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--				--%>
<%--				<g:if test="${prestadorServicoInstance?.cep}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="cep-label" class="property-label"><g:message code="prestadorServico.cep.label" default="Cep" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="cep-label"><g:fieldValue bean="${prestadorServicoInstance}" field="cep"/></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--			--%>
<%--				<g:if test="${prestadorServicoInstance?.estado}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="estado-label" class="property-label"><g:message code="prestadorServico.estado.label" default="Estado" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="estado-label"><g:link controller="estado" action="show" id="${prestadorServicoInstance?.estado?.id}">${prestadorServicoInstance?.estado?.encodeAsHTML()}</g:link></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--				--%>
<%--				<g:if test="${prestadorServicoInstance?.cidade}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="cidade-label" class="property-label"><g:message code="prestadorServico.cidade.label" default="Cidade" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="cidade-label"><g:link controller="cidade" action="show" id="${prestadorServicoInstance?.cidade?.id}">${prestadorServicoInstance?.cidade?.encodeAsHTML()}</g:link></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
				
<%-- ************************************** --%>
				
				<g:if test="${prestadorServicoInstance?.txtLogradouro}">
				<li class="fieldcontain">
					<span id="txtLogradouro-label" class="property-label"><g:message code="prestadorServico.txtLogradouro.label" default="Logradouro" /></span>
					
						<span class="property-value" aria-labelledby="txtLogradouro-label"><g:fieldValue bean="${prestadorServicoInstance}" field="txtLogradouro"/></span>
					
				</li>
				</g:if>
					
				<g:if test="${prestadorServicoInstance?.txtNumero}">
				<li class="fieldcontain">
					<span id="txtNumero-label" class="property-label"><g:message code="prestadorServico.txtNumero.label" default="Número" /></span>
					
						<span class="property-value" aria-labelledby="txtNumero-label"><g:fieldValue bean="${prestadorServicoInstance}" field="txtNumero"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${prestadorServicoInstance?.txtBairro}">
				<li class="fieldcontain">
					<span id="txtBairro-label" class="property-label"><g:message code="prestadorServico.txtBairro.label" default="Bairro" /></span>
					
						<span class="property-value" aria-labelledby="txtBairro-label"><g:fieldValue bean="${prestadorServicoInstance}" field="txtBairro"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${prestadorServicoInstance?.txtCEP}">
				<li class="fieldcontain">
					<span id="txtCEP-label" class="property-label"><g:message code="prestadorServico.txtCEP.label" default="CEP" /></span>
					
						<span class="property-value" aria-labelledby="txtCEP-label"><g:fieldValue bean="${prestadorServicoInstance}" field="txtCEP"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${prestadorServicoInstance?.txtEstado}">
				<li class="fieldcontain">
					<span id="txtEstado-label" class="property-label"><g:message code="prestadorServico.txtEstado.label" default="Estado" /></span>
					
						<span class="property-value" aria-labelledby="txtEstado-label"><g:fieldValue bean="${prestadorServicoInstance}" field="txtEstado"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${prestadorServicoInstance?.txtUF}">
				<li class="fieldcontain">
					<span id="txtUF-label" class="property-label"><g:message code="prestadorServico.txtUF.label" default="UF" /></span>
					
						<span class="property-value" aria-labelledby="txtUF-label"><g:fieldValue bean="${prestadorServicoInstance}" field="txtUF"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${prestadorServicoInstance?.txtCidade}">
				<li class="fieldcontain">
					<span id="txtCidade-label" class="property-label"><g:message code="prestadorServico.txtCidade.label" default="Cidade" /></span>
					
						<span class="property-value" aria-labelledby="txtCidade-label"><g:fieldValue bean="${prestadorServicoInstance}" field="txtCidade"/></span>
					
				</li>
				</g:if>
																
				<g:if test="${prestadorServicoInstance?.txtEndereco}">
				<li class="fieldcontain">
					<span id="txtEndereco-label" class="property-label"><g:message code="prestadorServico.txtEndereco.label" default="Endereço" /></span>
					
						<span class="property-value" aria-labelledby="txtEndereco-label"><g:fieldValue bean="${prestadorServicoInstance}" field="txtEndereco"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${prestadorServicoInstance?.txtLatitude}">
				<li class="fieldcontain">
					<span id="txtLatitude-label" class="property-label"><g:message code="prestadorServico.txtLatitude.label" default="Latitude" /></span>
					
						<span class="property-value" aria-labelledby="txtLatitude-label"><g:fieldValue bean="${prestadorServicoInstance}" field="txtLatitude"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${prestadorServicoInstance?.txtLongitude}">
				<li class="fieldcontain">
					<span id="txtLongitude-label" class="property-label"><g:message code="prestadorServico.txtLongitude.label" default="Longitude" /></span>
					
						<span class="property-value" aria-labelledby="txtLongitude-label"><g:fieldValue bean="${prestadorServicoInstance}" field="txtLongitude"/></span>
					
				</li>
				</g:if>
				
<%--				<g:if test="${prestadorServicoInstance?.txtRetornoCompleto}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="txtRetornoCompleto-label" class="property-label"><g:message code="prestadorServico.txtRetornoCompleto.label" default="Retorno Completo" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="txtRetornoCompleto-label"><g:fieldValue bean="${prestadorServicoInstance}" field="txtRetornoCompleto"/></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
				
				<g:if test="${prestadorServicoInstance?.txtLatitude}">			
					<div id="map-canvas"></div>
				</g:if>	
									
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${prestadorServicoInstance?.id}" />
					<g:link class="edit" action="edit" id="${prestadorServicoInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
		
		<div id="list-avaliacao" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entitiesAvaliacaoName]" /></h1>
			<br>
			<table>
				<thead>
					<tr>

						<th><g:message code="avaliacao.usuario.label" default="Usuário" /></th>
					
						<th><g:message code="avaliacao.avaliacao.label" default="Avaliação" /></th>
						
						<th><g:message code="avaliacao.comentario.label" default="Comentário" /></th>
						
						<th><g:message code="avaliacao.data.label" default="Data" /></th>
					
					</tr>
				</thead>
				<tbody>
					<g:each in="${avaliacaoInstanceList}" status="i" var="avaliacaoInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						
							<td>
								<g:link action="show" id="${avaliacaoInstance.id}">
									<g:if test="${avaliacaoInstance?.usuario.facebookUser}">
										<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small" src="https://graph.facebook.com/${avaliacaoInstance?.usuario.facebookUser}/picture?type=normal" /></div></span>
									</g:if>
									<g:else>
										<g:if test="${avaliacaoInstance?.usuario.avatar}">
											<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small" src="${createLink(controller:'usuario', action:'avatar_image', id:avaliacaoInstance?.usuario.id)}" /></div></span>					
					  					</g:if>
					  					<g:else>
					  						<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small default" src="/ServicosOnline/images/user-default.png" /></div></span>
					  					</g:else>
									</g:else>
									${fieldValue(bean: avaliacaoInstance, field: "usuario")}
								</g:link>
							</td>
	
							<td><div class="rating_div"><input value="${avaliacaoInstance.getMedia()}" type="number" class="rating" min=0 max=5 step=1 data-size="xs" disabled="disabled" ></div></td>

						<td class="comentario">${fieldValue(bean: avaliacaoInstance, field: "comentario")}</td>

						<td><g:formatDate date="${avaliacaoInstance.data}" type="datetime" style="SHORT"/></td>

					</tr>
				</g:each>
				</tbody>
			</table>

			<div class="pagination">
				<g:paginate total="${avaliacaoInstanceTotal}" />
			</div>
		</div>	
		
	</body>
</html>
