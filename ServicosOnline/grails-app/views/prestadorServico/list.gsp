
<%@ page import="br.com.servicosonline.PrestadorServico" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'prestadorServico.label', default: 'Prestador de Serviço')}" />
		<g:set var="entitiesName" value="${message(code: 'prestadoresServico.label', default: 'Prestadores de Serviço')}" />
		<title><g:message code="default.list.label" args="[entitiesName]" /></title>			
	</head>
	<body>
		<a href="#list-prestadorServico" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
				
		<div id="list-prestadorServico" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entitiesName]" /></h1>
			
			<br>
			<div class="nav">
				<g:form action="search" method="get">
		            <g:textField name="q" value="${params.q}" required="true"/>
		            <g:submitButton name="search" value="Buscar"/>
		        </g:form>
		    </div>
			<br>
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<g:each in="${prestadorServicoInstanceList}" status="i" var="prestadorServicoInstance">
				<g:render template="/prestadorServico/prestador" model="[prestadorServicoInstance: prestadorServicoInstance, i:i, destaque_class: '']" ></g:render>		
			</g:each>
				
<%--			</table>
			
<%--			<table>--%>
<%--				<thead>--%>
<%--					<tr>--%>
<%--					--%>
<%--						<g:sortableColumn property="nome" title="${message(code: 'prestadorServico.nome.label', default: 'Nome')}" />--%>
<%--					--%>
<%--						<g:sortableColumn property="email" title="${message(code: 'prestadorServico.email.label', default: 'E-mail')}" />--%>
<%--					--%>
<%--						<g:sortableColumn property="celular" title="${message(code: 'prestadorServico.celular.label', default: 'Celular')}" />--%>
<%--																--%>
<%--						<g:sortableColumn property="txtCidade" title="${message(code: 'prestadorServico.cidade.label', default: 'Cidade')}" />--%>
<%--					--%>
<%--						<th><g:message code="prestadorServico.avaliacao.label" default="Avaliação" /></th>--%>
<%--						--%>
<%--						<th><g:message code="prestadorServico.curtida.label" default="Curtida" /></th>--%>
<%--					--%>
<%--					</tr>--%>
<%--				</thead>--%>
<%--				<tbody>--%>
<%--				<g:each in="${prestadorServicoInstanceList}" status="i" var="prestadorServicoInstance">--%>
<%--					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">--%>
<%--					--%>
<%--						<td>--%>
<%--							<g:link action="show" id="${prestadorServicoInstance.id}">--%>
<%--								<g:if test="${prestadorServicoInstance?.avatar}">--%>
<%--									<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small" src="${createLink(controller:'prestadorServico', action:'avatar_image', id:prestadorServicoInstance?.id)}" /></div></span>					--%>
<%--				  				</g:if>--%>
<%--				  				<g:else>--%>
<%--				  					<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small default" src="/ServicosOnline/images/prestadorServico-default.png" /></div></span>--%>
<%--				  				</g:else>				  					--%>
<%--								${fieldValue(bean: prestadorServicoInstance, field: "nome")}--%>
<%--							</g:link>--%>
<%--						</td>--%>
<%--					--%>
<%--						<td style="text-align: center;">${fieldValue(bean: prestadorServicoInstance, field: "email")}</td>--%>
<%--					--%>
<%--						<td style="text-align: center;">${fieldValue(bean: prestadorServicoInstance, field: "celular")}</td>--%>
<%--					--%>
<%--						<td style="text-align: center;">${fieldValue(bean: prestadorServicoInstance, field: "txtCidade")}-${fieldValue(bean: prestadorServicoInstance, field: "txtUF")}</td>--%>
<%--						--%>
<%--						<td style="text-align: center;">--%>
<%--							<div class="rating_div">--%>
<%--								<input value="${prestadorServicoInstance.getMediaAvaliacoes()}" type="number" class="rating" min=0 max=5 step=1 data-size="xs" disabled="disabled" >--%>
<%--							</div>--%>
<%--							<g:if test="${!prestadorServicoInstance.jaAvaliou()}">--%>
<%--								<div class="avaliacao_div">--%>
<%--									<g:link controller="avaliacao" action="create" id="${prestadorServicoInstance.id}">Avaliar</g:link>--%>
<%--								 </div>	--%>
<%--							</g:if>--%>
<%--						</td>--%>
<%--						--%>
<%--						<td style="text-align: center;"><div id="curtir_${prestadorServicoInstance.id}"><g:render template="/prestadorServico/curtida" model="[prestadorServicoInstance: prestadorServicoInstance]" ></g:render></div></td>--%>
<%--					--%>
<%--					</tr>--%>
<%--				</g:each>--%>
<%--				</tbody>--%>
<%--			</table>--%>
			<div class="pagination">
				<g:paginate total="${prestadorServicoInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
