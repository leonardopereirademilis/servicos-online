<%@ page import="br.com.servicosonline.PrestadorServico" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'prestadorServico.label', default: 'Prestador de Serviço')}" />
		<g:set var="entitiesName" value="${message(code: 'prestadoresServico.label', default: 'Prestadores de Serviço')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
		<g:javascript library="mascaras"/>
		
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'estilo.css')}" type="text/css">
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script src="${resource(dir: 'js', file: 'jquery.min.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: 'mapa.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: 'jquery-ui.custom.min.js')}" type="text/javascript"></script>
        
	</head>
	<body>
		<a href="#create-prestadorServico" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entitiesName]" /></g:link></li>
			</ul>
		</div>
		<div id="create-prestadorServico" class="content scaffold-create" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${prestadorServicoInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${prestadorServicoInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
<%--			<g:form action="save" >--%>
			<g:uploadForm action="save">
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:uploadForm>	
<%--			</g:form>--%>
		</div>
	</body>
</html>
