<%@ page import="br.com.servicosonline.PrestadorServico" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'prestadorServico.label', default: 'Prestador de Serviço')}" />
		<g:set var="entitiesName" value="${message(code: 'prestadoresServico.label', default: 'Prestadores de Serviço')}" />
		<title><g:message code="default.search.label" args="[entitiesName]" /></title>		
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    	<meta charset="utf-8">
    	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    	<script>
			var map;
			function initialize(lat, lng, zoom) {
				var myLatLng = new google.maps.LatLng(lat, lng, zoom);
  				var mapOptions = {
    				zoom: zoom,
    				center: myLatLng
  				};
  				map = new google.maps.Map(document.getElementById('map-canvas'),
      				mapOptions);  	
			}

			function setMarker(lat, lng, zoom, title, imageURL){
				var image = 'http://localhost:8080' + imageURL;				
				var myLatLng = new google.maps.LatLng(lat, lng, zoom);
  			  	var marker = new google.maps.Marker({
  			    	position: myLatLng,
  			      	map: map,
  			      	icon: image,
  			      	title: title
  			  	});	
			}

			function setCenter(lat, lng, zoom){
				var myLatLng = new google.maps.LatLng(lat, lng, zoom);
				map.setCenter(myLatLng);
				location.hash = "map";
			}

    	</script>	
	</head>
	<body>
		<a href="#list-prestadorServico" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entitiesName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
				
		<div id="list-prestadorServico" class="content scaffold-list" role="main">
			<h1><g:message code="default.search.label" args="[entitiesName]" /></h1>
			
			<g:if test="${categoriaServicoInstance?.id}">
				<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
				</g:if>
				<ol class="property-list categoriaServico">
				
					<li class="fieldcontain">
						<span id="image-label" class="property-label"><g:message code="categoriaServico.icone.label" default="" /></span>
						<span class="property-value" aria-labelledby="image-label"><img class="icone_small" src="${createLink(action:'icone_image', controller:'categoriaServico', id:categoriaServicoInstance?.id)}" /></span>					
  					</li>
				
					<g:if test="${categoriaServicoInstance?.nome}">
					<li class="fieldcontain">
						<span id="nome-label" class="property-label"><g:message code="categoriaServico.nome.label" default="Nome" /></span>
						
							<span class="property-value" aria-labelledby="nome-label"><g:fieldValue bean="${categoriaServicoInstance}" field="nome"/></span>
						
					</li>
					</g:if>
				
					<g:if test="${categoriaServicoInstance?.quesitos}">
					<li class="fieldcontain">
						<span id="quesitos-label" class="property-label"><g:message code="categoriaServico.quesitos.label" default="Quesitos" /></span>
						
							<g:each in="${categoriaServicoInstance.quesitos}" var="q">
							<span class="property-value" aria-labelledby="quesitos-label"><g:link controller="quesito" action="show" id="${q.id}">${q?.nome}</g:link></span>
							</g:each>
						
					</li>
					</g:if>
				
				</ol>
				<g:form>
					<fieldset class="buttons">
						<g:hiddenField name="id" value="${categoriaServicoInstance?.id}" />
						<g:link class="edit" action="edit" id="${categoriaServicoInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
						<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
					</fieldset>
				</g:form>
			</g:if>	
			
			<br>
			<div class="nav">
				<g:form action="search" method="get">
		            <g:message code="categoriaServico.prestadorServico.label" default="Prestador" />
		            <g:textField name="q" value="${params.q}"/>
		            <g:hiddenField name="categoriaServicoId" value="${categoriaServicoInstance?.id}" />
		            		            
		            <g:message code="usuario.estado.label" default="Estado" />
					<g:select id="estado" name="estado.id" from="${br.com.servicosonline.Estado.list()}" optionKey="id" required="" value="${estado?.id}" class="many-to-one"  noSelection="['':'Selecione um estado...']" onchange="${remoteFunction(controller: 'cidade', action: 'buscaCidades', params: '\'estado=\' + this.value', update:'cidadeSelect')}" optionValue="nome"/>
				
					<g:message code="usuario.cidade.label" default="Cidade" />
					<div id="cidadeSelect" style="display: inline;">
						<g:render template="/cidade/cidade" model="[cidades: cidades, cidade:cidade?.id]" ></g:render>
					</div>
					
					<g:submitButton name="search" value="Buscar"/>
		        </g:form>
		    </div>
			<br>
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<g:set var="haveQuery" value="${params.q?.trim()}" />
			<g:set var="haveResults" value="${searchResult?.results}" />
			<g:set var="haveDestaqueResults" value="${searchDestaqueResult?.results}" />
						
			<g:if test="${haveDestaqueResults}">
				<div class="destaques"><span class="destaques_titulo"><g:message code="prestadorServico.destaques.patrocinados.label" default="Destaques Patrocinados" /></span>
					<g:set var="totalPages"	value="${Math.ceil(searchDestaqueResult.total / searchDestaqueResult.max)}" />
					<g:each in="${searchDestaqueResult.results}" status="i" var="prestadorServicoInstance">
						<g:if test="${(prestadorServicoInstance?.txtLatitude != null) && (prestadorServicoInstance?.txtLatitude != "")}">
							<script>
								google.maps.event.addDomListener(window, 'load', function() {
									if (${i} == 0){
										initialize(${prestadorServicoInstance?.txtLatitude}, ${prestadorServicoInstance?.txtLongitude}, 16);
									}
									setMarker(${prestadorServicoInstance?.txtLatitude}, ${prestadorServicoInstance?.txtLongitude}, 16, '${prestadorServicoInstance?.nome}', '${createLink(action:'icone_image', controller:'categoriaServico', id:categoriaServicoInstance?.id)}');
								});								
							</script>							
						</g:if>
						
						<g:render template="/prestadorServico/prestador" model="[prestadorServicoInstance: prestadorServicoInstance, i:i, destaque_class: ' destaque']" ></g:render>
					
					</g:each>
											
					<g:if test="${searchDestaqueResult.size() == 0}">
						<script>
							var latitudeDefault =  -27.5949884;
							var longitudeDefault = -48.54817430000003;
							var zoomDefault = 16;
														  
							google.maps.event.addDomListener(window, 'load', function() {
								initialize(latitudeDefault, longitudeDefault, 16);
								if(navigator.geolocation) {
									navigator.geolocation.getCurrentPosition(function(position) {
								    	latitudeDefault = position.coords.latitude;
								    	longitudeDefault = position.coords.longitude;    			
								    	initialize(latitudeDefault,longitudeDefault,16,false);
								    });
								}													
							});								
						</script>
					</g:if>
				</div>
			</g:if>
							
			<g:if test="${haveResults}">
				<g:set var="totalPages"	value="${Math.ceil(searchResult.total / Integer.parseInt(searchResult.max.toString()))}" />
				<g:each in="${searchResult.results}" status="i" var="prestadorServicoInstance">
					<g:if test="${(prestadorServicoInstance?.txtLatitude != null) && (prestadorServicoInstance?.txtLatitude != "")}">
						<script>
							google.maps.event.addDomListener(window, 'load', function() {
								if ((${i} == 0) && (${!haveDestaqueResults})){
									initialize(${prestadorServicoInstance?.txtLatitude}, ${prestadorServicoInstance?.txtLongitude}, 16);
								}
								setMarker(${prestadorServicoInstance?.txtLatitude}, ${prestadorServicoInstance?.txtLongitude}, 16, '${prestadorServicoInstance?.nome}', '${createLink(action:'icone_image', controller:'categoriaServico', id:categoriaServicoInstance?.id)}');
							});								
						</script>							
					</g:if>
					
					<g:render template="/prestadorServico/prestador" model="[prestadorServicoInstance: prestadorServicoInstance, i:i, destaque_class: '']" ></g:render>
					
				</g:each>
										
				<g:if test="${searchResult.size() == 0}">
					<script>
						var latitudeDefault =  -27.5949884;
						var longitudeDefault = -48.54817430000003;
						var zoomDefault = 16;
												  
						google.maps.event.addDomListener(window, 'load', function() {
							initialize(latitudeDefault, longitudeDefault, 16);
							if(navigator.geolocation) {
								navigator.geolocation.getCurrentPosition(function(position) {
							    	latitudeDefault = position.coords.latitude;
							    	longitudeDefault = position.coords.longitude;    			
							    	initialize(latitudeDefault,longitudeDefault,16,false);
							    });
							}													
						});								
					</script>
				</g:if>
			</g:if>
			<g:else>
				<g:if test="${!haveDestaqueResults}">
					<div class="no_results">
						<g:message code="search.no.result.label" default="Nenhum resultado encontrado..."/>
					</div>
				</g:if>	
			</g:else>				
			<div class="pagination">
				<g:paginate params="${params}" total="${searchResult.total}" />
			</div>
			
			<a name="map"></a>
			<div id="map-canvas"></div>
				
		</div>
	</body>
</html>