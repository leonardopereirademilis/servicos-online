<%@ page import="br.com.servicosonline.PrestadorServico" %>

<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'avatar', 'error')}">
	<label for="avatar">
		<g:message code="prestadorServico.avatar.label" default="Avatar (4MB)" />
		
	</label>
	<input type="file" name="avatar" id="avatar" />
</div>

<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="prestadorServico.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" value="${prestadorServicoInstance?.nome}" required="true"/>
</div>

<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'tipoPrestadorServico', 'error')} required">
	<label for="tipoPrestadorServico">
		<g:message code="prestadorServico.tipoPrestadorServico.label" default="Tipo Prestador Serviço" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="tipoPrestadorServico" name="tipoPrestadorServico.id" from="${br.com.servicosonline.TipoPrestadorServico.list()}" optionKey="id" value="${prestadorServicoInstance?.tipoPrestadorServico?.id}" class="many-to-one" required="true" noSelection="['':'Selecione um tipo...']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="prestadorServico.email.label" default="E-mail" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="email" value="${prestadorServicoInstance?.email}" required="true"/>
</div>

<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'celular', 'error')} ">
	<label for="celular">
		<g:message code="prestadorServico.celular.label" default="Celular" />
		
	</label>	
	<g:field name="celular" type="tel" value="${prestadorServicoInstance?.celular}" />
</div>

<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'telefoneResidencial', 'error')} ">
	<label for="telefoneResidencial">
		<g:message code="prestadorServico.telefoneResidencial.label" default="Telefone Residencial" />
		
	</label>
	<g:field name="telefoneResidencial" type="tel" value="${prestadorServicoInstance?.telefoneResidencial}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'telefoneComercial', 'error')} ">
	<label for="telefoneComercial">
		<g:message code="prestadorServico.telefoneComercial.label" default="Telefone Comercial" />
		
	</label>
	<g:field name="telefoneComercial" type="tel" value="${prestadorServicoInstance?.telefoneComercial}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'categoriasServico', 'error')} required">
	<label for="categoriasServico">
		<g:message code="prestadorServico.categoriasServico.label" default="Categorias Serviço" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="categoriasServico" from="${br.com.servicosonline.CategoriaServico.list()}" multiple="multiple" optionKey="id" size="5" value="${prestadorServicoInstance?.categoriasServico*.id}" class="many-to-many" required="true"/>
</div>

<%--<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'endereco', 'error')} ">--%>
<%--	<label for="endereco">--%>
<%--		<g:message code="prestadorServico.endereco.label" default="Endereço" />--%>
<%--		--%>
<%--	</label>--%>
<%--	<g:textField name="endereco" value="${prestadorServicoInstance?.endereco}" />--%>
<%--</div>--%>
<%----%>
<%--<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'numero', 'error')} ">--%>
<%--	<label for="numero">--%>
<%--		<g:message code="prestadorServico.numero.label" default="Número" />--%>
<%--		--%>
<%--	</label>--%>
<%--	<g:field name="numero" type="number" value="${prestadorServicoInstance.numero}" />--%>
<%--</div>--%>
<%--<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'bairro', 'error')} ">--%>
<%--	<label for="bairro">--%>
<%--		<g:message code="prestadorServico.bairro.label" default="Bairro" />--%>
<%--		--%>
<%--	</label>--%>
<%--	<g:textField name="bairro" value="${prestadorServicoInstance?.bairro}" />--%>
<%--</div>--%>
<%----%>
<%--<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'cep', 'error')} required">--%>
<%--	<label for="cep">--%>
<%--		<g:message code="prestadorServico.cep.label" default="CEP" />--%>
<%--		<span class="required-indicator">*</span>--%>
<%--	</label>--%>
<%--	<g:textField name="cep" value="${prestadorServicoInstance?.cep}" required="true"/>--%>
<%--</div>--%>
<%----%>
<%--<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'estado', 'error')} required">--%>
<%--	<label for="estado">--%>
<%--		<g:message code="prestadorServico.estado.label" default="Estado" />--%>
<%--		<span class="required-indicator">*</span>--%>
<%--	</label>--%>
<%--	<g:select id="estado" name="estado.id" from="${br.com.servicosonline.Estado.list()}" optionKey="id" required="" value="${prestadorServicoInstance?.estado?.id}" class="many-to-one"  noSelection="['':'Selecione um estado...']" onchange="${remoteFunction(controller: 'cidade', action: 'buscaCidades', params: '\'estado=\' + this.value', update:'cidadeSelect')}" />--%>
<%--</div>--%>
<%----%>
<%--<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'cidade', 'error')} required">--%>
<%--	<label for="cidade">--%>
<%--		<g:message code="prestadorServico.cidade.label" default="Cidade" />--%>
<%--		<span class="required-indicator">*</span>--%>
<%--	</label>--%>
<%--		<div id="cidadeSelect" style="display: inline;">--%>
<%--			<g:render template="/cidade/cidade" model="[cidades: cidades, cidade:prestadorServicoInstance?.cidade?.id]"  ></g:render>--%>
<%--		</div>--%>
<%--</div>--%>

<%-- ***** --%>

	<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'txtLogradouro', 'error')} disabled">
		<label for="txtLogradouro" class="disabled">
			<g:message code="prestadorServico.txtLogradouro.label" default="Logradouro" />
			
		</label>
		<g:textField name="txtLogradouroTF" value="${prestadorServicoInstance?.txtLogradouro}" disabled="true" />
		
	</div>
	
	<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'txtNumero', 'error')} disabled">
		<label for="txtNumero"  class="disabled">
			<g:message code="prestadorServico.txtNumero.label" default="Número" />
			
		</label>
		<g:textField name="txtNumeroTF" value="${prestadorServicoInstance?.txtNumero}" disabled="true" />
		
	</div>
	
	<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'txtBairro', 'error')} disabled">
		<label for="txtBairro" class="disabled">
			<g:message code="prestadorServico.txtBairro.label" default="Bairro" />
			
		</label>
		<g:textField name="txtBairroTF" value="${prestadorServicoInstance?.txtBairro}" disabled="true" />
		
	</div>
	
	<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'txtCEP', 'error')} disabled">
		<label for="txtCEP" class="disabled">
			<g:message code="prestadorServico.txtCEP.label" default="CEP" />
			
		</label>
		<g:textField name="txtCEPTF" value="${prestadorServicoInstance?.txtCEP}" disabled="true" />
		
	</div>
	
	
	<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'complemento', 'error')} ">
		<label for="complemento">
			<g:message code="prestadorServico.complemento.label" default="Complemento" />
			
		</label>
		<g:textField name="complemento" value="${prestadorServicoInstance?.complemento}" />
	</div>
		
	<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'txtEstado', 'error')} disabled">
		<label for="txtEstado" class="disabled">
			<g:message code="prestadorServico.txtEstado.label" default="Estado" />
			
		</label>
		<g:textField name="txtEstadoTF" value="${prestadorServicoInstance?.txtEstado}" disabled="true" />
		
	</div>
	
	<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'txtUF', 'error')} disabled">
		<label for="txtUF" class="disabled">
			<g:message code="prestadorServico.txtUF.label" default="UF" />
			
		</label>
		<g:textField name="txtUFTF" value="${prestadorServicoInstance?.txtUF}" disabled="true" />
		
	</div>
	
	<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'txtCidade', 'error')} disabled">
		<label for="txtCidade" class="disabled">
			<g:message code="prestadorServico.txtCidade.label" default="Cidade" />
			
		</label>
		<g:textField name="txtCidadeTF" value="${prestadorServicoInstance?.txtCidade}" disabled="true" />
		
	</div>	
<%--	String txtEndereco--%>
	
	<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'txtLatitude', 'error')} disabled">
		<label for="txtLatitude" class="disabled">
			<g:message code="prestadorServico.txtLatitude.label" default="Latitude" />
			
		</label>
		<g:textField name="txtLatitudeTF" value="${prestadorServicoInstance?.txtLatitude}" disabled="true" />
		
	</div>
	
	<div class="fieldcontain ${hasErrors(bean: prestadorServicoInstance, field: 'txtLongitude', 'error')} disabled">
		<label for="txtLongitude" class="disabled">
			<g:message code="prestadorServico.txtLongitude.label" default="Longitude" />
			
		</label>
		<g:textField name="txtLongitudeTF" value="${prestadorServicoInstance?.txtLongitude}" disabled="true" />
		
	</div>
	
<%--	String txtRetornoCompleto--%>
	            		                    		                    
	<div class="fieldcontain required">
		<label for="endereco">
			<g:message code="prestadorServico.endereco.label" default="Endereço" />
			<span class="required-indicator">*</span>									
		</label>
			<g:textField name="txtEndereco" id="txtEndereco" value="${prestadorServicoInstance?.txtEndereco}" required="true"/>
			<input type="button" id="btnEndereco" name="btnEndereco" value="Mostrar no mapa" />
	</div>
			
	<div id="mapa"></div>
	<g:if test="${(prestadorServicoInstance?.txtLatitude != null) && (prestadorServicoInstance?.txtLatitude != "")}">
		<script>initialize(${prestadorServicoInstance?.txtLatitude}, ${prestadorServicoInstance?.txtLongitude}, 16, false);</script>
	</g:if>
	<g:else>
		<script>
			initialize(latitudeDefault, longitudeDefault, zoomDefault, true);
		</script>
	</g:else>
			                
			                    
	<input type="hidden" id="txtLogradouro" name="txtLogradouro" value="${prestadorServicoInstance?.txtLogradouro}" />	
	<input type="hidden" id="txtNumero" name="txtNumero" value="${prestadorServicoInstance?.txtNumero}" />
	<input type="hidden" id="txtBairro" name="txtBairro" value="${prestadorServicoInstance?.txtBairro}" />
	<input type="hidden" id="txtCEP" name="txtCEP" value="${prestadorServicoInstance?.txtCEP}" />
	<input type="hidden" id="txtEstado" name="txtEstado" value="${prestadorServicoInstance?.txtEstado}" />
	<input type="hidden" id="txtUF" name="txtUF" value="${prestadorServicoInstance?.txtUF}" />
	<input type="hidden" id="txtCidade" name="txtCidade" value="${prestadorServicoInstance?.txtCidade}" />
<%--	<input type="hidden" id="txtEndereco" name="txtEndereco" value="${prestadorServicoInstance?.txtEndereco}" />--%>
	<input type="hidden" id="txtLatitude" name="txtLatitude" value="${prestadorServicoInstance?.txtLatitude}" />
	<input type="hidden" id="txtLongitude" name="txtLongitude" value="${prestadorServicoInstance?.txtLongitude}" />
	<input type="hidden" id="txtRetornoCompleto" name="txtRetornoCompleto" value="${prestadorServicoInstance?.txtRetornoCompleto}" />	                    
	

<%-- ***** --%>
