<%@page import="br.com.servicosonline.PrestadorServicoController"%>

<g:if test="${prestadorServicoInstance.jaCurtiu() == null}">
	<div class="curtir-btn" onclick="${remoteFunction(controller:'prestadorServico', action: 'receberCurtida', id: prestadorServicoInstance.id, update:'curtir_' + prestadorServicoInstance.id)}">
		${prestadorServicoInstance.getNuCurtidas()}
	</div>
	<div class="descurtir-btn" onclick="${remoteFunction(controller:'prestadorServico', action: 'receberDescurtida', id: prestadorServicoInstance.id, update:'curtir_' + prestadorServicoInstance.id)}">
		${prestadorServicoInstance.getNuDescurtidas()}
	</div>	
</g:if>	
<g:else>
	<g:if test="${prestadorServicoInstance.jaCurtiu()}">
		<div class="curtir-btn" onclick="${remoteFunction(controller:'prestadorServico', action: 'receberCurtida', id: prestadorServicoInstance.id, update:'curtir_' + prestadorServicoInstance.id)}">
			${prestadorServicoInstance.getNuCurtidas()}
		</div>
		<div class="descurtir-btn-disabled">
			${prestadorServicoInstance.getNuDescurtidas()}
		</div>
	</g:if>
	<g:else>
		<div class="curtir-btn-disabled">
			${prestadorServicoInstance.getNuCurtidas()}
		</div>
		<div class="descurtir-btn" onclick="${remoteFunction(controller:'prestadorServico', action: 'receberDescurtida', id: prestadorServicoInstance.id, update:'curtir_' + prestadorServicoInstance.id)}">
			${prestadorServicoInstance.getNuDescurtidas()}
		</div>
	</g:else>	
</g:else>	

