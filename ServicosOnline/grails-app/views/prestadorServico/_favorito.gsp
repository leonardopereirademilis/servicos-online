<%@page import="br.com.servicosonline.PrestadorServicoController"%>

<g:if test="${prestadorServicoInstance.ehFavorito()}">
	<div class="favorito-btn-disabled" onclick="${remoteFunction(controller:'prestadorServico', action: 'atualizarFavorito', id: prestadorServicoInstance.id, update:'favorito')}" title="Remover dos favoritos"></div>		
</g:if>
<g:else>
	<div class="favorito-btn" onclick="${remoteFunction(controller:'prestadorServico', action: 'atualizarFavorito', id: prestadorServicoInstance.id, update:'favorito')}" title="Adicionar aos favoritos"></div>
</g:else>