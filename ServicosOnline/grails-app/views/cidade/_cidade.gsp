<%@page import="br.com.servicosonline.PrestadorServicoController"%>

<g:if test="${cidades}">
	<g:select id="cidade" name="cidade.id" from="${cidades}" optionKey="id" required="" value="${cidade}" class="many-to-one" noSelection="['':'Selecione uma cidade...']" />
</g:if>
<g:else>
	<g:select id="cidade" name="cidade.id" from="" optionKey="id" required="" value="" class="many-to-one" noSelection="['':'Selecione um estado antes...']" disabled="false"/>
</g:else>