<%@ page import="br.com.servicosonline.Quesito" %>

<div class="fieldcontain ${hasErrors(bean: quesitoInstance, field: 'nome', 'error')} ">
	<label for="nome">
		<g:message code="quesito.nome.label" default="Nome" />
		
	</label>
	<g:textField name="nome" value="${quesitoInstance?.nome}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quesitoInstance, field: 'descricao', 'error')} ">
	<label for="descricao">
		<g:message code="quesito.descricao.label" default="Descrição" />
		
	</label>
	<g:textField name="descricao" value="${quesitoInstance?.descricao}"/>
</div>