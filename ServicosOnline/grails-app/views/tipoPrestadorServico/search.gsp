
<%@ page import="br.com.servicosonline.TipoPrestadorServico" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipoPrestadorServico.label', default: 'Tipo de Prestador de Serviço')}" />
		<g:set var="entitiesName" value="${message(code: 'tiposPrestadorServico.label', default: 'Tipos de Prestador de Serviço')}" />
		<title><g:message code="default.search.label" args="[entitiesName]" /></title>
	</head>
	<body>
		<a href="#list-tipoPrestadorServico" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entitiesName]" /></g:link></li>
				<sec:ifAnyGranted roles="ROLE_ADMIN">
					<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>	
			</ul>
		</div>
		<div id="list-tipoPrestadorServico" class="content scaffold-list" role="main">
			<h1><g:message code="default.search.label" args="[entitiesName]" /></h1>
			
			<br>
			<div class="nav">
				<g:form action="search" method="get">
		            <g:textField name="q" value="${params.q}" required="true"/>
		            <g:submitButton name="search" value="Buscar"/>
		        </g:form>
		    </div>
			<br>
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<g:set var="haveQuery" value="${params.q?.trim()}" />
			<g:set var="haveResults" value="${searchResult?.results}" />
	
			<g:if test="${haveResults}">
				<div class="list">
					<table>
						<thead>
							<tr>
							
								<g:sortableColumn property="tipo" title="${message(code: 'tipoPrestadorServico.tipo.label', default: 'Tipo')}" />
							
							</tr>
						</thead>
						<tbody>
						<g:set var="totalPages"	value="${Math.ceil(searchResult.total / searchResult.max)}" />
						<g:each in="${searchResult.results}" status="i" var="tipoPrestadorServicoInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							
								<td><g:link action="show" id="${tipoPrestadorServicoInstance.id}">${fieldValue(bean: tipoPrestadorServicoInstance, field: "tipo")}</g:link></td>
							
							</tr>
						</g:each>
						</tbody>
					</table>
				</div>
			</g:if>		
			<g:else>
				<div class="no_results">
					<g:message code="search.no.result.label" default="Nenhum resultado encontrado..."/>
				</div>
			</g:else>
			<div class="pagination">
				<g:paginate params="[q: params.q]" total="${searchResult.total}" />
			</div>
		</div>
	</body>
</html>
