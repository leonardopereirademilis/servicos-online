<%@ page import="br.com.servicosonline.TipoPrestadorServico" %>



<div class="fieldcontain ${hasErrors(bean: tipoPrestadorServicoInstance, field: 'tipo', 'error')} ">
	<label for="tipo">
		<g:message code="tipoPrestadorServico.tipo.label" default="Tipo" />
		
	</label>
	<g:textField name="tipo" value="${tipoPrestadorServicoInstance?.tipo}"/>
</div>

