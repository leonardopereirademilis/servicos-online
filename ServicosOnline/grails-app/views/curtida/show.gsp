
<%@ page import="br.com.servicosonline.Curtida" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'curtida.label', default: 'Curtida')}" />
		<g:set var="entitiesName" value="${message(code: 'curtidas.label', default: 'Curtidas')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-curtida" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entitiesName]" /></g:link></li>
				<sec:ifAnyGranted roles="ROLE_ADMIN">
					<li><g:link class="create" action="create"><g:message code="default.nova.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>
			</ul>
		</div>
		<div id="show-curtida" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list curtida">
			
				<g:if test="${curtidaInstance?.usuario}">
				<li class="fieldcontain">
					<span id="usuario-label" class="property-label"><g:message code="curtida.usuario.label" default="Usuario" /></span>
					
						<span class="property-value" aria-labelledby="usuario-label"><g:link controller="usuario" action="show" id="${curtidaInstance?.usuario?.id}">${curtidaInstance?.usuario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${curtidaInstance?.prestadorServico}">
				<li class="fieldcontain">
					<span id="prestadorServico-label" class="property-label"><g:message code="curtida.prestadorServico.label" default="Prestador Servico" /></span>
					
						<span class="property-value" aria-labelledby="prestadorServico-label"><g:link controller="prestadorServico" action="show" id="${curtidaInstance?.prestadorServico?.id}">${curtidaInstance?.prestadorServico?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${curtidaInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="curtida.status.label" default="Status" /></span>
					
						<span class="property-value" aria-labelledby="status-label"><g:formatBoolean boolean="${curtidaInstance?.status}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<sec:ifAnyGranted roles="ROLE_ADMIN">
				<g:form>
					<fieldset class="buttons">
						<g:hiddenField name="id" value="${curtidaInstance?.id}" />
						<g:link class="edit" action="edit" id="${curtidaInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
						<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
					</fieldset>
				</g:form>
			</sec:ifAnyGranted>	
		</div>
	</body>
</html>
