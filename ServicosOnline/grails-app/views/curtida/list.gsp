
<%@ page import="br.com.servicosonline.Curtida" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'curtida.label', default: 'Curtida')}" />
		<g:set var="entitiesName" value="${message(code: 'curtidas.label', default: 'Curtidas')}" />
		<title><g:message code="default.list.label" args="[entitiesName]" /></title>
	</head>
	<body>
		<a href="#list-curtida" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<sec:ifAnyGranted roles="ROLE_ADMIN">
					<li><g:link class="create" action="create"><g:message code="default.nova.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>	
			</ul>
		</div>
		<div id="list-curtida" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entitiesName]" /></h1>
			
			<br>
			<div class="nav">
				<g:form action="search" method="get">
		            <g:textField name="q" value="${params.q}" required="true"/>
		            <g:submitButton name="search" value="Buscar"/>
		        </g:form>
		    </div>
			<br>
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="curtida.usuario.label" default="Usuario" /></th>
					
						<th><g:message code="curtida.prestadorServico.label" default="Prestador Servico" /></th>
					
						<g:sortableColumn property="status" title="${message(code: 'curtida.status.label', default: 'Status')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${curtidaInstanceList}" status="i" var="curtidaInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td>
							<g:link action="show" id="${curtidaInstance.id}">
								<g:if test="${curtidaInstance?.usuario.facebookUser}">
									<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small" src="https://graph.facebook.com/${curtidaInstance?.usuario.facebookUser}/picture?type=normal" /></div></span>
								</g:if>
								<g:else>
									<g:if test="${curtidaInstance?.usuario.avatar}">
										<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small" src="${createLink(controller:'usuario', action:'avatar_image', id:curtidaInstance?.usuario.id)}" /></div></span>					
				  					</g:if>
				  					<g:else>
				  						<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small default" src="/ServicosOnline/images/user-default.png" /></div></span>
				  					</g:else>
				  				</g:else>	
								${fieldValue(bean: curtidaInstance, field: "usuario")}
							</g:link></td>
					
						<td style="text-align: center;"><g:link controller="prestadorServico" action="show" id="${curtidaInstance.prestadorServico.id}">${fieldValue(bean: curtidaInstance, field: "prestadorServico")}</g:link></td>
					
						<td style="text-align: center;">
							<g:if test="${curtidaInstance.status}">
								<div class="curtir-btn disabled"></div>
							</g:if>
							<g:else>
								<div class="descurtir-btn disabled"></div>
							</g:else>							
						</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${curtidaInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
