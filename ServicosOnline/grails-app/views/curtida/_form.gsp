<%@ page import="br.com.servicosonline.Curtida" %>



<div class="fieldcontain ${hasErrors(bean: curtidaInstance, field: 'usuario', 'error')} required">
	<label for="usuario">
		<g:message code="curtida.usuario.label" default="Usuario" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="usuario" name="usuario.id" from="${br.com.servicosonline.Usuario.list()}" optionKey="id" required="" value="${curtidaInstance?.usuario?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: curtidaInstance, field: 'prestadorServico', 'error')} required">
	<label for="prestadorServico">
		<g:message code="curtida.prestadorServico.label" default="Prestador Servico" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="prestadorServico" name="prestadorServico.id" from="${br.com.servicosonline.PrestadorServico.list()}" optionKey="id" required="" value="${curtidaInstance?.prestadorServico?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: curtidaInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="curtida.status.label" default="Status" />
		
	</label>
	<g:checkBox name="status" value="${curtidaInstance?.status}" />
</div>

