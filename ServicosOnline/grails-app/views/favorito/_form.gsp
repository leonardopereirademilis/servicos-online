<%@ page import="br.com.servicosonline.Favorito" %>



<div class="fieldcontain ${hasErrors(bean: favoritoInstance, field: 'usuario', 'error')} required">
	<label for="usuario">
		<g:message code="favorito.usuario.label" default="Usuario" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="usuario" name="usuario.id" from="${br.com.servicosonline.Usuario.list()}" optionKey="id" required="" value="${favoritoInstance?.usuario?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: favoritoInstance, field: 'prestadorServico', 'error')} required">
	<label for="prestadorServico">
		<g:message code="favorito.prestadorServico.label" default="Prestador Servico" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="prestadorServico" name="prestadorServico.id" from="${br.com.servicosonline.PrestadorServico.list()}" optionKey="id" required="" value="${favoritoInstance?.prestadorServico?.id}" class="many-to-one"/>
</div>

