
<%@ page import="br.com.servicosonline.Favorito" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'favorito.label', default: 'Favorito')}" />
		<g:set var="entitiesName" value="${message(code: 'favoritos.label', default: 'Favoritos')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    	<meta charset="utf-8">
    	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    	<script>
			var map;
			function initialize(lat, lng, zoom) {
				var myLatLng = new google.maps.LatLng(lat, lng, zoom);
  				var mapOptions = {
    				zoom: zoom,
    				center: myLatLng
  				};
  				map = new google.maps.Map(document.getElementById('map-canvas'),
      				mapOptions);  	
			}

			function setMarker(lat, lng, zoom, title, imageURL){
				var image = 'http://localhost:8080' + imageURL;				
				var myLatLng = new google.maps.LatLng(lat, lng, zoom);
  			  	var marker = new google.maps.Marker({
  			    	position: myLatLng,
  			      	map: map,
  			      	icon: image,
  			      	title: title
  			  	});	
			}

			function setCenter(lat, lng, zoom){
				var myLatLng = new google.maps.LatLng(lat, lng, zoom);
				map.setCenter(myLatLng);
				location.hash = "map";
			}

    	</script>
	</head>
	<body>
		<a href="#list-favorito" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<sec:ifAnyGranted roles="ROLE_ADMIN">
					<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>	
			</ul>
		</div>

		<div id="list-favorito" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entitiesName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:each in="${favoritoInstanceList}" status="i" var="favoritoInstance">
				<g:if test="${(favoritoInstance.prestadorServico?.txtLatitude != null) && (favoritoInstance.prestadorServico?.txtLatitude != "")}">
					<script>
						google.maps.event.addDomListener(window, 'load', function() {
							if (${i} == 0) {	
								initialize(${favoritoInstance.prestadorServico?.txtLatitude}, ${favoritoInstance.prestadorServico?.txtLongitude}, 16);
							}
							setMarker(${favoritoInstance.prestadorServico?.txtLatitude}, ${favoritoInstance.prestadorServico?.txtLongitude}, 16, '${favoritoInstance.prestadorServico?.nome}', '${createLink(controller:'categoriaServico', action:'icone_image', id:favoritoInstance.prestadorServico?.categoriasServico?.id)}');
						});								
					</script>					
					<g:render template="/prestadorServico/prestador" model="[prestadorServicoInstance: favoritoInstance.prestadorServico, i: i, destaque_class: '']" ></g:render>
				</g:if>			
			</g:each>

			<div class="pagination">
				<g:paginate total="${favoritoInstanceTotal}" />
			</div>
			
			<a name="map"></a>						
			<div id="map-canvas"></div>
			
		</div>
	</body>
</html>
