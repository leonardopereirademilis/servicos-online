<%@ page import="br.com.servicosonline.Estado" %>



<div class="fieldcontain ${hasErrors(bean: estadoInstance, field: 'nome', 'error')} ">
	<label for="nome">
		<g:message code="estado.nome.label" default="Estado" />
		
	</label>
	<g:textField name="nome" value="${estadoInstance?.nome}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: estadoInstance, field: 'pais', 'error')} required">
	<label for="pais">
		<g:message code="estado.pais.label" default="País" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="pais" name="pais.id" from="${br.com.servicosonline.Pais.list()}" optionKey="id" required="" value="${estadoInstance?.pais?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: estadoInstance, field: 'uf', 'error')} ">
	<label for="uf">
		<g:message code="estado.uf.label" default="Sigla" />
		
	</label>
	<g:textField name="uf" value="${estadoInstance?.uf}"/>
</div>

