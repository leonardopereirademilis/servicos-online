<%@ page import="br.com.servicosonline.Avaliacao" %>


<g:hiddenField name="usuario.id" id="usuario" value="${usuario.id}"/>
<g:if test="${prestadorServicoInstance?.id}">
	<g:hiddenField name="prestadorServico.id" id="prestadorServico" value="${prestadorServicoInstance?.id}"/>
	<div class="fieldcontain ${hasErrors(bean: avaliacaoInstance, field: 'prestadorServico', 'error')} required">
		<label for="prestadorServico">
			<g:message code="avaliacao.prestadorServico.label" default="Prestador Serviço" />
		</label>
		<g:link controller="prestadorServico" action="show" id="${prestadorServicoInstance?.id}">${prestadorServicoInstance?.nome}</g:link>
	</div>	
</g:if>
<g:else>
	<div class="fieldcontain ${hasErrors(bean: avaliacaoInstance, field: 'prestadorServico', 'error')} required">
		<label for="prestadorServico">
			<g:message code="avaliacao.prestadorServico.label" default="Prestador Serviço" />
			<span class="required-indicator">*</span>
		</label>
		<g:select id="prestadorServico" name="prestadorServico.id" from="${br.com.servicosonline.PrestadorServico.list()}" optionKey="id" required="" value="${avaliacaoInstance?.prestadorServico?.id}" class="many-to-one" noSelection="['':'Selecione um Prestador de Serviço...']" onchange="${remoteFunction(controller: 'avaliacao', action: 'create', params: '\'id=\' + this.value')}"/>
	</div>
</g:else>	
<g:hiddenField name="quesitos" value="${quesitos}"/>
<g:each in="${quesitos}" var="quesito" status="i">
	<div class="fieldcontain ${hasErrors(bean: avaliacaoInstance, field: 'quesitosAvaliados', 'error')} ">
		<label for="quesitosAvaliados">
			<g:message code="${quesito.nome}" default="${quesito.nome}" />			
		</label>
		<div class="rating_div">
			<g:if test="${quesitosAvaliados}">
				<input id="quesito_${quesito.id}" name="quesito_${quesito.id}" value="${quesitosAvaliados[i]?.valor}" type="number" class="rating" min=0 max=5 step=1 data-size="xs" >
			</g:if>
			<g:else>
				<input id="quesito_${quesito.id}" name="quesito_${quesito.id}" value="0" type="number" class="rating" min=0 max=5 step=1 data-size="xs" >
			</g:else>
		</div>	
	</div>
</g:each>

<div class="fieldcontain ${hasErrors(bean: avaliacaoInstance, field: 'comentario', 'error')} ">
	<label for="comentario">
		<g:message code="avaliacao.comentario.label" default="Comentário" />
		
	</label>
	<g:textArea name="comentario" value="${avaliacaoInstance?.comentario}" maxlength="255"/>
</div>