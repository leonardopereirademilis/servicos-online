
<%@ page import="br.com.servicosonline.Avaliacao" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'avaliacao.label', default: 'Avaliação')}" />
		<g:set var="entitiesName" value="${message(code: 'avaliacoes.label', default: 'Avaliações')}" />
		<title><g:message code="default.list.label" args="[entitiesName]" /></title>
	</head>
	<body>
		<a href="#list-avaliacao" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<sec:ifAnyGranted roles="ROLE_ADMIN">
					<li><g:link class="create" action="create"><g:message code="default.nova.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>	
			</ul>
		</div>
		<div id="list-avaliacao" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entitiesName]" /></h1>
			
			<br>
			<div class="nav">
				<g:form action="search" method="get">
		            <g:textField name="q" value="${params.q}" required="true"/>
		            <g:submitButton name="search" value="Buscar"/>
		        </g:form>
		    </div>
			<br>
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="avaliacao.usuario.label" default="Usuário" /></th>
					
						<th><g:message code="avaliacao.prestadorServico.label" default="Prestador de Serviço" /></th>
						
						<th><g:message code="avaliacao.avaliacao.label" default="Avaliação" /></th>
						
						<th><g:message code="avaliacao.comentario.label" default="Comentário" /></th>
						
						<th><g:message code="avaliacao.data.label" default="Data" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${avaliacaoInstanceList}" status="i" var="avaliacaoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td>
							<g:link action="show" id="${avaliacaoInstance.id}">
								<g:if test="${avaliacaoInstance?.usuario.facebookUser}">
									<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small" src="https://graph.facebook.com/${avaliacaoInstance?.usuario.facebookUser}/picture?type=normal" /></div></span>
								</g:if>
								<g:else>
									<g:if test="${avaliacaoInstance?.usuario.avatar}">
										<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small" src="${createLink(controller:'usuario', action:'avatar_image', id:avaliacaoInstance?.usuario.id)}" /></div></span>					
				  					</g:if>
				  					<g:else>
				  						<span class="property-value" aria-labelledby="image-label"><div class="avatar_small_div"><img class="avatar_small default" src="/ServicosOnline/images/user-default.png" /></div></span>
				  					</g:else>
				  				</g:else>	
								${fieldValue(bean: avaliacaoInstance, field: "usuario")}
							</g:link>
						</td>
					
						<td style="text-align: center;">${fieldValue(bean: avaliacaoInstance, field: "prestadorServico")}</td>
						
						<td style="text-align: center;"><div class="rating_div"><input value="${avaliacaoInstance.getMedia()}" type="number" class="rating" min=0 max=5 step=1 data-size="xs" disabled="disabled" ></div></td>
						
						<td class="comentario">${fieldValue(bean: avaliacaoInstance, field: "comentario")}</td>
						
						<td style="text-align: center;"><g:formatDate date="${avaliacaoInstance.data}" type="datetime" style="SHORT"/></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>

			<div class="pagination">
				<g:paginate total="${avaliacaoInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
