
<%@ page import="br.com.servicosonline.Avaliacao" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'avaliacao.label', default: 'Avaliação')}" />
		<g:set var="entitiesName" value="${message(code: 'avaliacoes.label', default: 'Avaliações')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>		
	</head>
	<body>
		<a href="#show-avaliacao" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="home" controller="categoriaServico" action="list"><g:message code="default.home.label"/></g:link></li>
				<li><g:link class="favorito" controller="favorito" action="list" params="[byuser:1]"><g:message code="default.favorito.label" default="Favoritos"/></g:link></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entitiesName]" /></g:link></li>
				<sec:ifAnyGranted roles="ROLE_ADMIN">
					<li><g:link class="create" action="create"><g:message code="default.nova.label" args="[entityName]" /></g:link></li>
				</sec:ifAnyGranted>	
			</ul>
		</div>
		<div id="show-avaliacao" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list avaliacao">
			
				<g:if test="${avaliacaoInstance?.usuario}">
				<li class="fieldcontain">
					<span id="usuario-label" class="property-label"><g:message code="avaliacao.usuario.label" default="Usuário" /></span>
					
						<span class="property-value" aria-labelledby="usuario-label"><g:link controller="usuario" action="show" id="${avaliacaoInstance?.usuario?.id}">${avaliacaoInstance?.usuario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${avaliacaoInstance?.prestadorServico}">
				<li class="fieldcontain">
					<span id="prestadorServico-label" class="property-label"><g:message code="avaliacao.prestadorServico.label" default="Prestador Serviço" /></span>
					
						<span class="property-value" aria-labelledby="prestadorServico-label"><g:link controller="prestadorServico" action="show" id="${avaliacaoInstance?.prestadorServico?.id}">${avaliacaoInstance?.prestadorServico?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
				
				<g:if test="${avaliacaoInstance?.data}">
				<li class="fieldcontain">
					<span id="data-label" class="property-label"><g:message code="avaliacao.data.label" default="Data da Avaliação" /></span>
					
						<span class="property-value" aria-labelledby="data-label"><g:formatDate date="${avaliacaoInstance?.data}" type="datetime" style="MEDIUM"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${avaliacaoInstance?.quesitosAvaliados}">
				<li class="fieldcontain">
					<span id="quesitosAvaliados-label" class="property-label"><g:message code="avaliacao.quesitosAvaliados.label" default="Quesitos Avaliados" /></span>
						<g:each in="${avaliacaoInstance.quesitosAvaliados}" var="q">
							<span class="property-value" aria-labelledby="quesitosAvaliados-label">
								<g:link controller="quesitoAvaliado" action="show" id="${q.id}">${q?.encodeAsHTML()}</g:link>
								<input value="${q?.valor}" type="number" class="rating" min=0 max=5 step=1 data-size="xs" disabled="disabled" >
							</span>								
						</g:each>
				</li>
				</g:if>
				
				<g:if test="${avaliacaoInstance?.comentario}">
				<li class="fieldcontain">
					<span id="data-label" class="property-label"><g:message code="avaliacao.comentario.label" default="Comentário" /></span>
					
						<span class="property-value" aria-labelledby="comentario-label">${avaliacaoInstance?.comentario}</span>
					
				</li>
				</g:if>
			
			</ol>
			<sec:ifAnyGranted roles="ROLE_ADMIN">
				<g:form>
					<fieldset class="buttons">
						<g:hiddenField name="id" value="${avaliacaoInstance?.id}" />
						<g:link class="edit" action="edit" id="${avaliacaoInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
						<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
					</fieldset>
				</g:form>
			</sec:ifAnyGranted>	
		</div>
	</body>
</html>
