<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="${resource(dir: 'js', file: 'star-rating.js')}" type="text/javascript"></script>
	    		
		<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" >
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'star-rating.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'star-rating.min.css')}" type="text/css">		
		
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
		<g:layoutHead/>
		<r:layoutResources />
		
<%--		<script type="text/javascript">--%>
			
			
<%--			var latitudeDefault;--%>
<%--			var longitudeDefault;--%>
<%--			var zoomDefault;--%>
					
<%--			var latitudeDefault =  -27.5949884;--%>
<%--	    	var longitudeDefault = -48.54817430000003;--%>
<%--			var zoomDefault = 16;--%>
<%--		--%>
<%--			alert(latitudeDefault);--%>
<%--			if (latitudeDefault == 'undefined') {--%>
<%--				alert("AAA"); --%>
<%--				--%>
<%--			--%>
<%--		    	if(navigator.geolocation) {--%>
<%--					navigator.geolocation.getCurrentPosition(function(position) {--%>
<%--						latitudeDefault = position.coords.latitude;--%>
<%--						longitudeDefault = position.coords.longitude;--%>
<%--						initialize(latitudeDefault,longitudeDefault,16);--%>
<%--					});--%>
<%--				}--%>
<%--			}--%>
<%-- 		</script>--%>
		
	</head>
	<body>
<%--		<div id="grailsLogo" role="banner"><a href="${createLink(uri: '/')}"><img src="${resource(dir: 'images', file: 'grails_logo.png')}" alt="Grails"/></a><div id="loginHeader"><g:loginControl /></div></div>--%>
		<div id="grailsLogo" role="banner"><g:link controller="categoriaServico" action="list"><img src="${resource(dir: 'images', file: 'grails_logo.png')}" alt="Grails"/></g:link><div id="loginHeader"><g:loginControl /></div></div>
		<g:layoutBody/>
		<div class="footer" role="contentinfo">© Copyright 2015-${Calendar.instance.get(Calendar.YEAR)} Serviços Online</div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>
