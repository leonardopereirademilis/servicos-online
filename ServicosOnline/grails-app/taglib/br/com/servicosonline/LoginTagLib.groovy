package br.com.servicosonline

import org.springframework.security.core.context.SecurityContextHolder

class LoginTagLib {
	def loginControl = {
		 if (SecurityContextHolder.getContext().getAuthentication().authorities.toString() != '[ROLE_ANONYMOUS]'){
			 Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)			 
			 out << """<a href="/ServicosOnline/usuario/show/${usuario.id}" class="loginTagLink">"""
			 
			 if (session.SPRING_SECURITY_CONTEXT?.authentication.hasProperty('uid')){
				 out << """<div class="avatar_small_div"><img class="avatar_small" src="https://graph.facebook.com/${usuario.facebookUser}/picture?type=normal"/></div>"""
			 }else{ 
				 if (usuario.avatar){
					out << """<div class="avatar_small_div"><img class="avatar_small" src="/ServicosOnline/usuario/avatar_image/${usuario.id}"/></div>"""
				 }else{
				 	out << """<div class="avatar_small_div"><img class="avatar_small" src="/ServicosOnline/images/user-default.png"/></div>"""
				 }
		 	 }	 
			 
			 out << "${usuario.nome}</a>"
			 out << """ [${link(action:"index", controller:"logout"){"Logout"}}]"""
		 } else {
		 	 out << """ [${link(action:"auth", controller:"login"){"Login"}}]"""
		 }
	}
}
