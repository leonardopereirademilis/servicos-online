import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler

// Place your Spring DSL code here
beans = {
	redirectFailureHandlerExample(SimpleUrlAuthenticationFailureHandler) {
		defaultFailureUrl = '/login/auth' //redirect to this URL when authentication fails		
	}
	
	redirectSuccessHandlerExample(SimpleUrlAuthenticationSuccessHandler) {
		defaultTargetUrl = '/login/auth' //redirect to this URL when authentication success
//		defaultSuccessUrl = '/login/auth' //redirect to this URL when authentication success
	}
	
}
