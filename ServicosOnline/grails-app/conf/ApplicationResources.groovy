modules = {
    application {
        resource url:'js/application.js'
    }
	
	maskedinput {
		dependsOn 'core'
		resource url:'js/maskedinput.js'
	}
	
	mascaras {
		dependsOn 'maskedinput'
		resource url:'js/mascaras.js'
	}
}