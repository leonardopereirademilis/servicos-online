package br.com.servicosonline

class Estado {
	
	static searchable = true

    String nome
	String uf
	
	static belongsTo = [pais:Pais]

    static constraints = {
    }
	
	String toString() {
		nome
	}
}