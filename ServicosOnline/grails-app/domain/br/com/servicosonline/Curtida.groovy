package br.com.servicosonline

class Curtida {
	
	static searchable = true

	Usuario usuario
	PrestadorServico prestadorServico
	Boolean status
	
	static mapping = {
	}
		
    static constraints = {
		usuario unique: 'prestadorServico'
    }	
}
