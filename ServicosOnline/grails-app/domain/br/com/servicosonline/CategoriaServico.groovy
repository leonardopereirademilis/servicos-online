package br.com.servicosonline

class CategoriaServico {
	
	static searchable = true
	
	String nome
	
	byte[] icone
	String iconeType
	
	static hasMany = [quesitos: Quesito]

    static constraints = {
		icone(nullable:true, maxSize: 4096000 /* 4000K */)
		iconeType(nullable:true)
    }
	
	String toString() {
		nome
	}
}
