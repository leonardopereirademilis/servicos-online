package br.com.servicosonline

class Pais {
	
	static searchable = true

    String nome
	String name
	
	static hasMany = [estados:Estado]

    static constraints = {
		nome(unique:true)
		name(unique:true)
    }
	
	String toString() {
		nome
	}
}