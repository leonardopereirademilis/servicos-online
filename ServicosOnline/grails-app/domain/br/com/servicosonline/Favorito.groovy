package br.com.servicosonline

class Favorito {
	
	static searchable = true

	Usuario usuario
	PrestadorServico prestadorServico
	
    static constraints = {
		usuario unique: 'prestadorServico'
    }
}
