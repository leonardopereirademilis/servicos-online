package br.com.servicosonline

class Usuario {

	transient springSecurityService
	
	static searchable = true

	String username
	String password
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
	
	String nome
	String email
	String endereco
	String complemento
	Integer numero
	String bairro
	String cep
	Estado estado
	Cidade cidade
	String telefoneResidencial
	String telefoneComercial
	String celular
	
	String facebookUser
		
	byte[] avatar
	String avatarType
		
	static hasMany = [usuarios: Usuario, usuariosNegados: Usuario]

	static constraints = {
		username blank: false, unique: true
		password blank: false
		facebookUser nullable:true
		
		email(nullable:true)
		cidade(nullable:true)
		estado(nullable:true)
		nome(nullable:true)
		
		
		endereco(nullable:true)
		complemento(nullable:true)
		numero(nullable:true)
		bairro(nullable:true)
		cep(nullable:true)
		telefoneResidencial(nullable:true)
		telefoneComercial(nullable:true)
		celular(nullable:true)
		
		email(unique:true)
//		password(password:true)
		avatar(nullable:true, maxSize: 4096000 /* 4000K */)
		avatarType(nullable:true)
	}

	static mapping = {
		password column: '`password`'		
	}

	Set<Papel> getAuthorities() {
		UsuarioPapel.findAllByUsuario(this).collect { it.papel } as Set
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}
	
	String toString() {
		nome
	}
			
	Integer verificarRelacionamento(){
		UsuarioController usuarioController = new UsuarioController()
		return usuarioController.verificarRelacionamento(id)
	}	
}
