package br.com.servicosonline

class Quesito {
	
	static searchable = true
	
	String nome
	String descricao

    static constraints = {
    }
	
	String toString() {
		id
	}
}
