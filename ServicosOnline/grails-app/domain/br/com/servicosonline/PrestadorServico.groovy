package br.com.servicosonline

class PrestadorServico {
	
	static searchable = true
		
	String nome
	TipoPrestadorServico tipoPrestadorServico
	String email
//	String endereco
	String complemento
//	Integer numero
//	String bairro
//	String cep
//	Estado estado
//	Cidade cidade
	String telefoneResidencial
	String telefoneComercial
	String celular
	
	String txtLogradouro	
	String txtNumero
	String txtBairro
	String txtCEP
	String txtEstado
	String txtUF
	String txtCidade	
	String txtEndereco
	String txtLatitude
	String txtLongitude
	String txtRetornoCompleto
	
	BigDecimal media
	
	Integer destaque
	
	byte[] avatar
	String avatarType
				
	static hasMany = [categoriasServico: CategoriaServico]

    static constraints = {
		complemento(nullable:true)
		telefoneResidencial(nullable:true)
		telefoneComercial(nullable:true)
		celular(nullable:true)
		media(nullable:true)
		
		avatar(nullable:true, maxSize: 4096000 /* 4000K */)
		avatarType(nullable:true)
    }
	
	String toString() {
		nome
	}
	
	Boolean jaCurtiu(){
		PrestadorServicoController prestadorServicoController = new PrestadorServicoController()		
		return prestadorServicoController.jaCurtiu(id)
	}
	
	Boolean jaAvaliou(){
		PrestadorServicoController prestadorServicoController = new PrestadorServicoController()		
		return prestadorServicoController.jaAvaliou(id)
	}
	
	Number getNuCurtidas() {
		PrestadorServicoController prestadorServicoController = new PrestadorServicoController()
		return prestadorServicoController.getNuCurtidas(id)		 
	}
	
	Number getNuDescurtidas() {
		PrestadorServicoController prestadorServicoController = new PrestadorServicoController()
		return prestadorServicoController.getNuDescurtidas(id)
	}
	
	Number getMediaAvaliacoes() {
		PrestadorServicoController prestadorServicoController = new PrestadorServicoController()
		return prestadorServicoController.getMediaAvaliacoes(id)
	}
	
	Boolean ehFavorito(){
		PrestadorServicoController prestadorServicoController = new PrestadorServicoController()
		return prestadorServicoController.ehFavorito(id)
	}
}
