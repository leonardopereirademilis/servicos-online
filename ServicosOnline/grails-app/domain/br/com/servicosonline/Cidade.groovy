package br.com.servicosonline

class Cidade {
	
	static searchable = true

	String nome
	
	static belongsTo = [estado: Estado]

    static constraints = {
    }
	
	String toString() {
		nome
	}
}