package br.com.servicosonline

class Avaliacao {
	
	static searchable = true
	
	Usuario usuario
	PrestadorServico prestadorServico
	Date data
	String comentario
	
	static hasMany = [quesitosAvaliados: QuesitoAvaliado]

    static constraints = {
		usuario unique: 'prestadorServico'
		comentario(nullable:true)
    }
	
	static mapping = {
	}
	
	def getMedia(){
		AvaliacaoController avaliacaoController = new AvaliacaoController()
		return avaliacaoController.getMedia(id)
	}
}
