package br.com.servicosonline

class TipoPrestadorServico {
	
	static searchable = true

	String tipo
	
    static constraints = {
		tipo(unique:true)
    }
	
	String toString() {
		tipo
	}
}
