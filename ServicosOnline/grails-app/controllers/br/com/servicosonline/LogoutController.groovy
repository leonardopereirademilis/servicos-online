package br.com.servicosonline
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.springframework.security.access.annotation.Secured

class LogoutController {

	/**
	 * Index action. Redirects to the Spring security logout uri.
	 */
	def index = {
		// TODO put any pre-logout code here
		redirect uri: SpringSecurityUtils.securityConfig.logout.filterProcessesUrl // '/j_spring_security_logout'
	}
	
	@Secured(['ROLE_ADMIN'])
	def create (){
		
	}
	
	@Secured(['ROLE_ADMIN'])
	def edit (){
		
	}
}
