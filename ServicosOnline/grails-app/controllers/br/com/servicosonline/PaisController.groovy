package br.com.servicosonline

import org.springframework.security.access.annotation.Secured
import org.springframework.security.core.authority.GrantedAuthorityImpl
import org.springframework.security.core.context.SecurityContextHolder

class PaisController {
    static scaffold = true
	
	def beforeInterceptor = [action:this.&auth] //, except:["index", "list", "show"]]
	
	def auth() {
		if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new GrantedAuthorityImpl('ROLE_ANONYMOUS'))){
			redirect(uri: "/login/auth")
			return false
		}
	}
	
	@Secured(['ROLE_ADMIN'])
	def create (){
		
	}
	
	@Secured(['ROLE_ADMIN'])
	def edit (){
		
	}
}
