package br.com.servicosonline

import org.compass.core.engine.SearchEngineQueryParseException
import org.springframework.security.core.authority.GrantedAuthorityImpl
import org.springframework.security.core.context.SecurityContextHolder


class UsuarioController {
    static scaffold = true
	
	private static final okcontents = ['image/png', 'image/jpeg', 'image/gif', 'image/jpg']
	
	def beforeInterceptor = [action:this.&auth, except:["index", "login", "logout", "create", "save"]]
	
	def auth() {
		if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new GrantedAuthorityImpl('ROLE_ANONYMOUS'))){
			redirect(uri: "/login/auth")
			return false
		}
	}
	
	def logout = {
		redirect(uri: "/logout/index")
	}
		
	static String WILDCARD = "*"
	def searchableService
	
	/**
	 * Index page with search form and results
	 */
	def search = {
		if (!params.q?.trim()) {
			return [:]
		}
		try {
			String searchTerm = WILDCARD + params.q + WILDCARD
			return [searchResult: Usuario.search(searchTerm, params)]
			//return [searchResult: searchableService.search(params.q, params)]
		} catch (SearchEngineQueryParseException ex) {
			return [parseException: true]
		}
	}

	/**
	 * Perform a bulk index of every searchable object in the database
	 */
	def indexAll = {
		Thread.start {
			searchableService.index()
		}
		render("bulk index started in a background thread")
	}

	/**
	 * Perform a bulk index of every searchable object in the database
	 */
	def unindexAll = {
		searchableService.unindex()
		render("unindexAll done")
	}
	
	def save(){
								
		if (Usuario.findByEmail(params.email)){
			flash.message = "E-mail j&aacute; cadastrado"
			redirect(action:params.view, params: params)
			return
		}
		
		params.username = params.email
		params.enabled = true
		
		Usuario usuarioInstance = new Usuario(params)
				
		if (usuarioInstance.avatar){
			// Get the avatar file from the multi-part request
			def f = request.getFile('avatar')
	
			// List of OK mime-types
			if (!okcontents.contains(f.getContentType())) {
				flash.message = "O Avatar deve ser: ${okcontents}"
				//def cidades = Cidade.findAllByEstado(usuarioInstance.estado)
				params.password = ""
				
				redirect(action:params.view, params: params)
				return
			}
	
			// Save the image and mime type
			usuarioInstance.avatar = f.bytes
			usuarioInstance.avatarType = f.contentType
			log.info("File uploaded: $usuarioInstance.avatarType")
		}
		
		if(!usuarioInstance.avatarType){
			usuarioInstance.avatar = null
		}
										
		// Validation works, will check if the image is too big
		if (!usuarioInstance.save(flush: true)) {
			//def cidades = Cidade.findAllByEstado(usuarioInstance.estado)
			params.password = ""
			flash.message = "Usu&aacute;rio n&atilde;o criado"
			redirect(action:params.view, params: params)				
			return
		}
		
		Papel papel_usuario = Papel.findByAuthority('ROLE_USER')
		UsuarioPapel.create(usuarioInstance, papel_usuario)
								
		if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new GrantedAuthorityImpl('ROLE_ANONYMOUS'))){		
			redirect(uri: "/login/auth")		
		}else{
			redirect(action:show, id: usuarioInstance.id)
		}
	} 
	
	def update(){
		Usuario usuarioInstance = Usuario.get(params.id)
						
		usuarioInstance.nome = params.nome
		usuarioInstance.email = params.email
	
		Usuario usuarioAux = new Usuario(username:params.email, password:params.password, enabled:true)
		usuarioInstance.password = usuarioAux.password
		usuarioInstance.endereco = params.endereco
		usuarioInstance.complemento = params.complemento
		usuarioInstance.numero = Integer.parseInt(params.numero)
		usuarioInstance.bairro = params.bairro
		usuarioInstance.cep = params.cep		
		usuarioInstance.estado = Estado.get(params.get("estado.id"))
		usuarioInstance.cidade = Cidade.get(params.get("cidade.id"))
		usuarioInstance.telefoneResidencial = params.telefoneResidencial
		usuarioInstance.telefoneComercial = params.telefoneComercial
		usuarioInstance.celular = params.celular
	
		usuarioInstance.save(flush: true)
		
		try {
			// Get the avatar file from the multi-part request
			def f = request.getFile('avatar')
			
			if (!f.empty) {
				// List of OK mime-types
				if (!okcontents.contains(f.getContentType())) {
					flash.message = "O Avatar deve ser: ${okcontents}"
					redirect(action:params.view, id: params.id)
					return
				}
			
				// Save the image and mime type
				usuarioInstance.avatar = f.bytes
				usuarioInstance.avatarType = f.contentType
				log.info("File uploaded: $usuarioInstance.avatarType")
						
				// Validation works, will check if the image is too big
				if (!usuarioInstance.save(flush: true)) {
					redirect(action:params.view, id: params.id)
					return
				}
			}	
		}		
		catch (Exception e) {
			flash.message = "Avatar (${usuarioInstance.avatarType}, ${usuarioInstance.avatar.size()} bytes) n�o enviado."
			redirect(action:params.view, id: params.id)									
		}	
				
		redirect(action:show, id: params.id)
	}
	
	def relacionarUsuario() {
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
		Usuario usuarioRelacionado = Usuario.get(params.id)
				
		usuario.usuarios.add(usuarioRelacionado)
		usuario.usuariosNegados.remove(usuarioRelacionado)
		usuario.save()
						
		render(template: "relacionar", model: [usuarioInstance: usuarioRelacionado, usuario: usuario])
	}
	
	def naoRelacionarUsuario() {
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
		Usuario usuarioRelacionado = Usuario.get(params.id)
				
		usuario.usuariosNegados.add(usuarioRelacionado)
		usuario.usuarios.remove(usuarioRelacionado)
		usuario.save()
								
		render(template: "relacionar", model: [usuarioInstance: usuarioRelacionado, usuario: usuario])
	}
	
	def verificarRelacionamento(usuarioRelacionadoId) {
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
		Usuario usuarioRelacionado = Usuario.get(usuarioRelacionadoId)
		
		int relacionamento = 0 
		
		if ((usuario.usuarios.contains(usuarioRelacionado)) || (usuarioRelacionado.usuariosNegados.contains(usuario))) {
			relacionamento += 1
		}
		
		if (usuarioRelacionado.usuarios.contains(usuario)){
			relacionamento += 2
		}
		
		if (usuario.usuariosNegados.contains(usuarioRelacionado)){
			relacionamento += 2
		}
				 
		return relacionamento
	}
	
	def list(Integer max) {
		params.max = Math.min(max ?: 10, 100)
				
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
				
		def criteria = Usuario.createCriteria()
		def usuarioInstanceList = criteria.list(params) {
			ne("id", usuario.id)
		}
		
		[usuarioInstanceList: usuarioInstanceList,	usuarioInstanceTotal: usuarioInstanceList.totalCount, usuario: usuario]
	}
		
	def upload_avatar() {
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
				
		def usuarioInstance = Usuario.get(usuario.id)

		// Get the avatar file from the multi-part request
		def f = request.getFile('avatar')

		// List of OK mime-types
		if (!okcontents.contains(f.getContentType())) {
			flash.message = "Avatar must be one of: ${okcontents}"
			render(view:'select_avatar', model:[usuarioInstance:usuarioInstance])
			return
		}

		// Save the image and mime type
		usuarioInstance.avatar = f.bytes
		usuarioInstance.avatarType = f.contentType
		log.info("File uploaded: $usuarioInstance.avatarType")

		// Validation works, will check if the image is too big
		if (!usuarioInstance.save()) {
			render(view:'select_avatar', model:[usuarioInstance:usuarioInstance])
			return
		}

		flash.message = "Avatar (${usuarioInstance.avatarType}, ${usuarioInstance.avatar.size()} bytes) uploaded."
		redirect(action:'show')
	}
	
	def avatar_image() {
		def avatarUsuarioInstance = Usuario.get(params.id)
		if (!avatarUsuarioInstance || !avatarUsuarioInstance.avatar || !avatarUsuarioInstance.avatarType) {
			response.sendError(404)
			return
		}
		response.contentType = avatarUsuarioInstance.avatarType
		response.contentLength = avatarUsuarioInstance.avatar.size()
		OutputStream out = response.outputStream
		out.write(avatarUsuarioInstance.avatar)
		out.close()
	}
	
	def select_avatar={		
	}
	
	def create(){
		Usuario usuario
		if (!SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new GrantedAuthorityImpl('ROLE_ANONYMOUS'))){
			if (!SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new GrantedAuthorityImpl('ROLE_ADMIN'))){
				redirect(uri: "/login/denied")
				return
			}
			usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
		}
		
		if (params.estado != null){
			def estadoInstance = Estado.get(params['estado.id'])
			params.estado = estadoInstance
		}
		
		if (params.cidade != null){
			def cidadeInstance = Cidade.get(params['cidade.id'])
			params.cidade = cidadeInstance
		}
		
		def usuarioInstance = new Usuario(params)
		def cidades = Cidade.findAllByEstado(usuarioInstance.estado)
		
		[usuarioInstance: usuarioInstance, cidades:cidades, usuario: usuario]		
	}
	
	def edit(){
		
		if(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id.toString().trim() != params.id.toString().trim()){
			if (!SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new GrantedAuthorityImpl('ROLE_ADMIN'))){
				redirect(uri: "/login/denied")
				return
			}
		}		
			
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
						
		if (params.estado != null){
			def estadoInstance = Estado.get(params['estado.id'])
			params.estado = estadoInstance
		}
		
		if (params.cidade != null){
			def cidadeInstance = Cidade.get(params['cidade.id'])
			params.cidade = cidadeInstance
		}
		
		def usuarioInstance = Usuario.get(params.id)
		
		if (usuarioInstance == null){
			redirect(uri: "/login/denied")
			return
		}
		
		def cidades = Cidade.findAllByEstado(usuarioInstance.estado)
		
		[usuarioInstance: usuarioInstance, cidades:cidades, usuario: usuario]
	}
	
	def delete(){
				
		if ((params.id == null) || (params._action_delete == null)){
			redirect(uri: "/login/denied")
			return
		}
		
		if(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id.toString().trim() != params.id.toString().trim()){
			if (!SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new GrantedAuthorityImpl('ROLE_ADMIN'))){
				redirect(uri: "/login/denied")
				return
			}
		}
		
		Usuario usuarioInstance = Usuario.get(params.id)
		
		try{
			usuarioInstance.delete()
			flash.message = "Usu&aacute removido com sucesso"
		}catch (Exception e) {
			flash.message = "Usu&aacute n&atilde;o removido"
			redirect(action:'list')
			return
		}
				
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
						
		if (usuario.id == usuarioInstance.id){
			redirect(uri: "/logout/index")
			return
		}else{
			redirect(action:'list')
			return
		}
		
	
	}	
}