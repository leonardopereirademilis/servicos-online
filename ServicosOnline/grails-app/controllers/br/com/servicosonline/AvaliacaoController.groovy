package br.com.servicosonline

import org.compass.core.engine.SearchEngineQueryParseException
import org.springframework.security.access.annotation.Secured
import org.springframework.security.core.authority.GrantedAuthorityImpl
import org.springframework.security.core.context.SecurityContextHolder

class AvaliacaoController {
    static scaffold = true
	
	def beforeInterceptor = [action:this.&auth] //, except:["index", "list", "show"]]
	
	def auth() {
		if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new GrantedAuthorityImpl('ROLE_ANONYMOUS'))){
			redirect(uri: "/login/auth")
			return false
		}
	}
		
//	@Secured(['ROLE_ADMIN'])
	def create (){
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
				
		def prestadorServico = PrestadorServico.get(params.id)
		List<Quesito> quesitos = new ArrayList<Quesito>()
		
		if (prestadorServico){
			for (categoriaServico in prestadorServico.categoriasServico) {
				for (quesito in categoriaServico.quesitos) {
					if (!quesitos.contains(quesito)) {
						quesitos.add(quesito)
					}				
				}						 
			}
		}		 
		
		[prestadorServicoInstance: prestadorServico, quesitos: quesitos, usuario: usuario]
	}
	
//	@Secured(['ROLE_ADMIN'])
	def edit (){
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
		
		def avaliacao = Avaliacao.get(params.id)		
		List<Quesito> quesitos = new ArrayList<Quesito>()
		List<QuesitoAvaliado> quesitosAvaliados = new ArrayList<QuesitoAvaliado>()
		
		if (avaliacao){
			for (quesitoAvaliado in avaliacao.quesitosAvaliados) {
				quesitos.add(quesitoAvaliado.quesito)
				quesitosAvaliados.add(quesitoAvaliado)
			}
		}
						
		[avaliacaoInstance: avaliacao, prestadorServicoInstance: avaliacao.prestadorServico, quesitos: quesitos, quesitosAvaliados: quesitosAvaliados, usuario: usuario]
	}
	
	def save(){		
		def prestadorServico = PrestadorServico.get(params.prestadorServico.id)
		
		def avaliacao = new Avaliacao()
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
		avaliacao.usuario = usuario
		avaliacao.prestadorServico = prestadorServico
		avaliacao.data = new Date()
		avaliacao.comentario = params.comentario
		avaliacao.save()
		
		String quesitos = params.quesitos
		quesitos = quesitos.substring(1, quesitos.size() - 1)
		def listaQuesitos = quesitos.split(",")
								
		for(def i = 0; i < listaQuesitos.size() ;i++){
			def quesitoAvaliado = new QuesitoAvaliado()
			quesitoAvaliado.avaliacao = avaliacao
			quesitoAvaliado.quesito = Quesito.get(listaQuesitos[i].trim())
			quesitoAvaliado.valor = Integer.parseInt(params.get("quesito_" + listaQuesitos[i].trim()))
			quesitoAvaliado.save()
		}
		
		prestadorServico.media = avaliacao.prestadorServico.getMediaAvaliacoes()
		prestadorServico.save() 
					
		redirect(controller:"avaliacao", action:"show", id:avaliacao.id)
	}
	
	def update(){
		def avaliacao = Avaliacao.get(Integer.parseInt(params.id))
		avaliacao.data = new Date()
		avaliacao.comentario = params.comentario
		avaliacao.save()
		
		String quesitos = params.quesitos
		quesitos = quesitos.substring(1, quesitos.size() - 1)
		def listaQuesitos = quesitos.split(",")
	
		listaQuesitos.eachWithIndex { quesito, index ->
			def quesitoAvaliado = QuesitoAvaliado.findByAvaliacaoAndQuesito(avaliacao, Quesito.get(quesito))
			quesitoAvaliado.valor = Integer.parseInt(params.get("quesito_" + quesito.trim()))
		}							
			
		avaliacao.prestadorServico.media = avaliacao.prestadorServico.getMediaAvaliacoes()
		avaliacao.prestadorServico.save()
				
		redirect(controller:"avaliacao", action:"show", id:avaliacao.id)
	}
	
	def getMedia(id) {
		
		def criteria = QuesitoAvaliado.createCriteria()
		def media = criteria.list(){

			avaliacao{
				eq('id', id)
			}
			
			projections {
				avg ('valor')
			}
		}
				
		if(media.get(0) == null){
			return 0
		}
		
		def inteiro = Math.floor(media.get(0))
		def decimal = media.get(0)%1
		
		if(decimal < 0.25){  
			decimal = 0
		}else if((decimal >= 0.25) && (decimal < 0.75)){
			decimal = 0.5
		}else{
			decimal = 1
		}
				
		return inteiro + decimal
	}
	
	static String WILDCARD = "*"
	def searchableService
	
	/**
	 * Index page with search form and results
	 */
	def search = {
		if (!params.q?.trim()) {
			return [:]
		}
		try {
			String searchTerm = WILDCARD + params.q + WILDCARD
			return [searchResult: Avaliacao.search(searchTerm, params)]
			//return [searchResult: searchableService.search(params.q, params)]
		} catch (SearchEngineQueryParseException ex) {
			return [parseException: true]
		}
	}

	/**
	 * Perform a bulk index of every searchable object in the database
	 */
	def indexAll = {
		Thread.start {
			searchableService.index()
		}
		render("bulk index started in a background thread")
	}

	/**
	 * Perform a bulk index of every searchable object in the database
	 */
	def unindexAll = {
		searchableService.unindex()
		render("unindexAll done")
	}
}
