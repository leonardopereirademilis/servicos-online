package br.com.servicosonline

import org.compass.core.engine.SearchEngineQueryParseException
import org.springframework.security.core.authority.GrantedAuthorityImpl
import org.springframework.security.core.context.SecurityContextHolder

class PrestadorServicoController {
    static scaffold = true
	
	private static final okcontents = ['image/png', 'image/jpeg', 'image/gif', 'image/jpg']
	
	def beforeInterceptor = [action:this.&auth] //, except:["index", "list", "show"]]
	
	def auth() {
		if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new GrantedAuthorityImpl('ROLE_ANONYMOUS'))){
			redirect(uri: "/login/auth")
			return false
		}
	}
		
	def getNuCurtidas(id) {
		def prestadorServicoInstance = PrestadorServico.get(id)
		def curtidaInstanceTotal = Curtida.countByPrestadorServicoAndStatus(prestadorServicoInstance, true)
		return curtidaInstanceTotal
	}
	
	def getNuDescurtidas(id) {
		def prestadorServicoInstance = PrestadorServico.get(id)
		def descurtidaInstanceTotal = Curtida.countByPrestadorServicoAndStatus(prestadorServicoInstance, false)
		return descurtidaInstanceTotal
	}
	
	def receberCurtida() {
		def prestadorServico = PrestadorServico.get(params.id)
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)		
		def curtida = Curtida.findByUsuarioAndPrestadorServicoAndStatus(usuario, prestadorServico, true)
		
		if(curtida){
			curtida.delete(flush: true)
		}else{
			curtida = new Curtida()
			curtida.usuario = usuario
			curtida.prestadorServico = prestadorServico
			curtida.status = true
			curtida.save()			
		}
		
		render(template: "curtida", model: [prestadorServicoInstance: prestadorServico])
	}
	
	def receberDescurtida() {
		def prestadorServico = PrestadorServico.get(params.id)
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
		def descurtida = Curtida.findByUsuarioAndPrestadorServicoAndStatus(usuario, prestadorServico, false)
		
		if(descurtida){
			descurtida.delete(flush: true)
		}else{
			descurtida = new Curtida()
			descurtida.usuario = usuario
			descurtida.prestadorServico = prestadorServico
			descurtida.status = false
			descurtida.save()			
		}

		render(template: "curtida", model: [prestadorServicoInstance: prestadorServico])
	}
	
	def jaCurtiu(id) {
		def prestadorServico = PrestadorServico.get(id)
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
		def curtida = Curtida.findByUsuarioAndPrestadorServico(usuario, prestadorServico)

		if(curtida){
			return curtida.status
		}else{
			return null
		}	
	}
	
	def jaAvaliou(id) {
		def prestadorServico = PrestadorServico.get(id)
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
		def avaliacao = Avaliacao.findByUsuarioAndPrestadorServico(usuario, prestadorServico)

		if(avaliacao){
			return true
		}else{
			return false
		}
	}
	
	def getMediaAvaliacoes(id) {
		def criteria = QuesitoAvaliado.createCriteria()
		def media = criteria.list(){

			avaliacao{
				prestadorServico{
					eq('id', id)
				}
			}
			
			projections {
				avg ('valor')
			}
		}
				
		if(media.get(0) == null){
			return 0
		}
		
		def inteiro = Math.floor(media.get(0))
		def decimal = media.get(0)%1
		
		if(decimal < 0.25){  
			decimal = 0
		}else if((decimal >= 0.25) && (decimal < 0.75)){
			decimal = 0.5
		}else{
			decimal = 1
		}
				
		return inteiro + decimal
	}
	
	static String WILDCARD = "*"
	def searchableService
	
	/**
	 * Index page with search form and results
	 */
	def search = {
		
//		if (!params.q?.trim()) {
//			return [:]
//		}
		try {
			CategoriaServico categoriaServicoInstance
			String searchTerm = WILDCARD + params.q + WILDCARD
			def searchResult = [:]
			def searchDestaqueResult = [:]
									
			def estado = Estado.get(params.get("estado.id"))
			def cidade = Cidade.get(params.get("cidade.id"))
			
			if (params.categoriaServicoId){
				categoriaServicoInstance = CategoriaServico.get(params.categoriaServicoId)
										
//				searchResult = PrestadorServico.search(searchTerm, params){
//					categoriasServico{
//						eq("id", Long.parseLong(params.categoriaServicoId))
//					}
//				}
//****								
				def prestadorServicoCriteria = PrestadorServico.createCriteria()
				def prestadorServicoList = prestadorServicoCriteria.list(max: params.max?:10, offset: params.offset?:0){
					or {
						for (q in params.q.split(" ")) {
							like("nome", "%" + q + "%")
						} 
					}					
					
					categoriasServico{
						eq("id", Long.parseLong(params.categoriaServicoId))
					}
										
					eq("txtEstado", estado.nome)
					
					eq("txtCidade", cidade.nome)
					
					eq("destaque", 0)
					
					order("media", "desc")
					order("nome", "asc")
				}
				
				searchResult.total = prestadorServicoList.totalCount
				searchResult.hits = null
				searchResult.max = params.max?:10
				searchResult.scores = null
				searchResult.results = prestadorServicoList
				searchResult.offset = params.offset?:0
				
				def prestadorServicoDestaqueCriteria = PrestadorServico.createCriteria()
				def prestadorServicoDestaqueList = prestadorServicoDestaqueCriteria.list(){
					or {
						for (q in params.q.split(" ")) {
							like("nome", "%" + q + "%")
						} 
					}					
					
					categoriasServico{
						eq("id", Long.parseLong(params.categoriaServicoId))
					}
										
					eq("txtEstado", estado.nome)
					
					eq("txtCidade", cidade.nome)
					
					gt("destaque", 0)
					
					order("destaque", "asc")
					order("nome", "asc")
				}
				
				searchDestaqueResult.total = prestadorServicoDestaqueList.totalCount
				searchDestaqueResult.hits = null
				searchDestaqueResult.max = params.max?:10
				searchDestaqueResult.scores = null
				searchDestaqueResult.results = prestadorServicoDestaqueList
				searchDestaqueResult.offset = params.offset?:0
				
//****				
			}else{				
				searchResult = PrestadorServico.search(searchTerm, params)
			}						
			
			def cidades = Cidade.findAllByEstado(estado)
			
			return [searchResult: searchResult, categoriaServicoInstance: categoriaServicoInstance, estado:estado, cidade:cidade, cidades:cidades, searchDestaqueResult: searchDestaqueResult]

		} catch (SearchEngineQueryParseException ex) {
			return [parseException: true]
		}
	}

	/**
	 * Perform a bulk index of every searchable object in the database
	 */
	def indexAll = {
		Thread.start {
			searchableService.index()
		}
		render("bulk index started in a background thread")
	}

	/**
	 * Perform a bulk index of every searchable object in the database
	 */
	def unindexAll = {
		searchableService.unindex()
		render("unindexAll done")
	}
	
	def show(){
		
		def prestadorServicoInstance = PrestadorServico.get(params.id)
								
		def avaliacaoCriteria = Avaliacao.createCriteria()
		def avaliacaoInstanceList = avaliacaoCriteria.list(max: params.max?:10, offset: params.offset?:0){
			prestadorServico{
				eq("id", Long.parseLong(params.id))
			}
		}
	
		[prestadorServicoInstance: prestadorServicoInstance, avaliacaoInstanceList: avaliacaoInstanceList, avaliacaoInstanceTotal:avaliacaoInstanceList.totalCount]
	}
	
	def ehFavorito(id) {
		def prestadorServico = PrestadorServico.get(id)
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
		def favorito = Favorito.findByUsuarioAndPrestadorServico(usuario, prestadorServico)

		if(favorito){
			return true
		}else{
			return false
		}
	}
	
	def atualizarFavorito() {
		def prestadorServico = PrestadorServico.get(params.id)
		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
		def favorito = Favorito.findByUsuarioAndPrestadorServico(usuario, prestadorServico)
		
		if(favorito){
			favorito.delete(flush: true)
		}else{
			favorito = new Favorito()
			favorito.usuario = usuario
			favorito.prestadorServico = prestadorServico
			favorito.save()
		}

		render(template: "favorito", model: [prestadorServicoInstance: prestadorServico])
	}
	
//	def upload_avatar() {
//		Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
//				
//		def usuarioInstance = Usuario.get(usuario.id)
//
//		// Get the avatar file from the multi-part request
//		def f = request.getFile('avatar')
//
//		// List of OK mime-types
//		if (!okcontents.contains(f.getContentType())) {
//			flash.message = "Avatar must be one of: ${okcontents}"
//			render(view:'select_avatar', model:[usuarioInstance:usuarioInstance])
//			return
//		}
//
//		// Save the image and mime type
//		usuarioInstance.avatar = f.bytes
//		usuarioInstance.avatarType = f.contentType
//		log.info("File uploaded: $usuarioInstance.avatarType")
//
//		// Validation works, will check if the image is too big
//		if (!usuarioInstance.save()) {
//			render(view:'select_avatar', model:[usuarioInstance:usuarioInstance])
//			return
//		}
//
//		flash.message = "Avatar (${usuarioInstance.avatarType}, ${usuarioInstance.avatar.size()} bytes) uploaded."
//		redirect(action:'show')
//	}
	
	def avatar_image() {
		def avatarPrestadorServicoInstance = PrestadorServico.get(params.id)
		if (!avatarPrestadorServicoInstance || !avatarPrestadorServicoInstance.avatar || !avatarPrestadorServicoInstance.avatarType) {
			response.sendError(404)
			return
		}
		response.contentType = avatarPrestadorServicoInstance.avatarType
		response.contentLength = avatarPrestadorServicoInstance.avatar.size()
		OutputStream out = response.outputStream
		out.write(avatarPrestadorServicoInstance.avatar)
		out.close()
	}
	
	def select_avatar={
	}
	
	def save(){
		if (PrestadorServico.findByEmail(params.email)){
			flash.message = "E-mail j&aacute; cadastrado"
//			redirect(action:create, params: params)
			redirect(uri: "/prestadorServico/create")
			return
		}
		
		PrestadorServico prestadorServicoInstance = new PrestadorServico(params)

		if (prestadorServicoInstance.avatar){
			// Get the avatar file from the multi-part request
			def f = request.getFile('avatar')

			// List of OK mime-types
			if (!okcontents.contains(f.getContentType())) {
				flash.message = "O Avatar deve ser: ${okcontents}"
				
//				redirect(action:create, params: params)
				redirect(uri: "/prestadorServico/create")
				return
			}

			// Save the image and mime type
			prestadorServicoInstance.avatar = f.bytes
			prestadorServicoInstance.avatarType = f.contentType
			log.info("File uploaded: $prestadorServicoInstance.avatarType")
		}

		if(!prestadorServicoInstance.avatarType){
			prestadorServicoInstance.avatar = null
		}

		// Validation works, will check if the image is too big
		if (!prestadorServicoInstance.save(flush: true)) {
			flash.message = "Prestador de Servi&ccedil;o n&atilde;o criado"
//			redirect(action:create, params: params)
			redirect(uri: "/prestadorServico/create")
			return
		}
		
		//redirect(action:show, id: prestadorServicoInstance.id)	
		redirect(uri: "/prestadorServico/show/" + prestadorServicoInstance.id)
		return
	}
	
	def update(){
		PrestadorServico prestadorServicoInstance = PrestadorServico.get(params.id)
		prestadorServicoInstance.nome = params.nome
		prestadorServicoInstance.tipoPrestadorServico = TipoPrestadorServico.get(params.get("tipoPrestadorServico.id"))
		prestadorServicoInstance.email = params.email
		prestadorServicoInstance.complemento = params.complemento
		prestadorServicoInstance.telefoneResidencial = params.telefoneResidencial
		prestadorServicoInstance.telefoneComercial = params.telefoneComercial
		prestadorServicoInstance.celular = params.celular
		prestadorServicoInstance.txtLogradouro = params.txtLogradouro
		prestadorServicoInstance.txtNumero = params.txtNumero
		prestadorServicoInstance.txtBairro = params.txtBairro
		prestadorServicoInstance.txtCEP = params.txtCEP
		prestadorServicoInstance.txtEstado = params.txtEstado
		prestadorServicoInstance.txtUF = params.txtUF
		prestadorServicoInstance.txtCidade = params.txtCidade
		prestadorServicoInstance.txtEndereco = params.txtEndereco
		prestadorServicoInstance.txtLatitude = params.txtLatitude
		prestadorServicoInstance.txtLongitude = params.txtLongitude
		prestadorServicoInstance.txtRetornoCompleto = params.txtRetornoCompleto
				
		prestadorServicoInstance.save(flush: true)
		
		try {
			// Get the avatar file from the multi-part request
			def f = request.getFile('avatar')
			
			if (!f.empty) {
				// List of OK mime-types
				if (!okcontents.contains(f.getContentType())) {
					flash.message = "O Avatar deve ser: ${okcontents}"
					redirect(uri: "/prestadorServico/edit/" + params.id)
					return
				}
			
				// Save the image and mime type
				prestadorServicoInstance.avatar = f.bytes
				prestadorServicoInstance.avatarType = f.contentType
				log.info("File uploaded: $prestadorServicoInstance.avatarType")
						
				// Validation works, will check if the image is too big
				if (!prestadorServicoInstance.save(flush: true)) {
					redirect(uri: "/prestadorServico/edit/" + params.id)
					return
				}
			}
		}
		catch (Exception e) {
			flash.message = "Avatar (${prestadorServicoInstance.avatarType}, ${prestadorServicoInstance.avatar.size()} bytes) n&atilde;o enviado."
			redirect(uri: "/prestadorServico/edit/" + params.id)
			return
		}
				
		redirect(uri: "/prestadorServico/show/" + params.id)
		return
	}
}
