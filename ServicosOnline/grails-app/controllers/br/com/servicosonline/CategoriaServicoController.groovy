package br.com.servicosonline

import org.compass.core.engine.SearchEngineQueryParseException
import org.springframework.security.access.annotation.Secured
import org.springframework.security.core.authority.GrantedAuthorityImpl
import org.springframework.security.core.context.SecurityContextHolder

class CategoriaServicoController {
    static scaffold = true
	
	private static final okcontents = ['image/png', 'image/jpeg', 'image/gif']
	
	def beforeInterceptor = [action:this.&auth] //, except:["index", "list", "show"]]

	def auth() {
		if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new GrantedAuthorityImpl('ROLE_ANONYMOUS'))){
			redirect(uri: "/login/auth")
			return false
		}
	}
	
	@Secured(['ROLE_ADMIN'])
	def create (){
		
	}
	
	@Secured(['ROLE_ADMIN'])
	def edit (){
		
	}
	
	static String WILDCARD = "*"
	def searchableService
	
	/**
	 * Index page with search form and results
	 */
	def search = {
		if (!params.q?.trim()) {
			return [:]
		}
		try {
			String searchTerm = WILDCARD + params.q + WILDCARD
			return [searchResult: CategoriaServico.search(searchTerm, params)]
			//return [searchResult: searchableService.search(params.q, params)]
		} catch (SearchEngineQueryParseException ex) {
			return [parseException: true]
		}
	}

	/**
	 * Perform a bulk index of every searchable object in the database
	 */
	def indexAll = {
		Thread.start {
			searchableService.index()
		}
		render("bulk index started in a background thread")
	}

	/**
	 * Perform a bulk index of every searchable object in the database
	 */
	def unindexAll = {
		searchableService.unindex()
		render("unindexAll done")
	}
	
	def show(){
		Usuario usuarioInstance = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
		
		def categoriaServicoInstance = CategoriaServico.get(params.id)
								
		def prestadorServicoCriteria = PrestadorServico.createCriteria()
		def prestadorServicoInstanceList = prestadorServicoCriteria.list(max: params.max?:10, offset: params.offset?:0){
			categoriasServico{
				eq("id", Long.parseLong(params.id))
			}
			
			eq("txtEstado", usuarioInstance.estado.nome)
			
			eq("txtCidade", usuarioInstance.cidade.nome)
			
			eq("destaque", 0)
			
			order("media", "desc")
			order("nome", "asc")
		}
						
		def prestadorServicoCriteriaAll = PrestadorServico.createCriteria()
		def prestadorServicoInstanceListAll = prestadorServicoCriteriaAll.list{
			categoriasServico{
				eq("id", Long.parseLong(params.id))
			}
			
			eq("txtEstado", usuarioInstance.estado.nome)
			
			eq("txtCidade", usuarioInstance.cidade.nome)
			
			order("media", "desc")
			order("nome", "asc")
		}
		
		def cidades = Cidade.findAllByEstado(usuarioInstance.estado)
		
		def prestadorServicoDestaqueCriteria = PrestadorServico.createCriteria()
		def prestadorServicoDestaqueInstanceList = prestadorServicoDestaqueCriteria.list(){
			categoriasServico{
				eq("id", Long.parseLong(params.id))
			}
			
			eq("txtEstado", usuarioInstance.estado.nome)
			
			eq("txtCidade", usuarioInstance.cidade.nome)
			
			gt("destaque", 0)
			
			order("destaque", "asc")
			order("nome", "asc")
		}
	
		[categoriaServicoInstance: categoriaServicoInstance, prestadorServicoInstanceList: prestadorServicoInstanceList, prestadorServicoInstanceTotal:prestadorServicoInstanceList.totalCount, prestadorServicoInstanceListAll: prestadorServicoInstanceListAll, usuarioInstance: usuarioInstance, cidades: cidades, prestadorServicoDestaqueInstanceList: prestadorServicoDestaqueInstanceList, prestadorServicoDestaqueInstanceListTotal: prestadorServicoDestaqueInstanceList.size()]
	}
	
	def save(){
		CategoriaServico categoriaServicoInstance = new CategoriaServico(params)
		
		if (categoriaServicoInstance.icone){
			// Get the avatar file from the multi-part request
			def f = request.getFile('icone')
	
			// List of OK mime-types
			if (!okcontents.contains(f.getContentType())) {
				flash.message = "O &iacute;cone deve ser: ${okcontents}"
				
				redirect(action:params.view, params: params)
				return
			}
	
			// Save the image and mime type
			categoriaServicoInstance.icone = f.bytes
			categoriaServicoInstance.iconeType = f.contentType
			log.info("File uploaded: $categoriaServicoInstance.iconeType")
		}
		
		// Validation works, will check if the image is too big
		if (!categoriaServicoInstance.save()) {
			redirect(action:params.view, params: params)
			return
		}
				
		redirect(action:list)
	}
	
	def icone_image() {
		def iconeCategoriaServicoInstance = CategoriaServico.get(params.id)
		if (!iconeCategoriaServicoInstance || !iconeCategoriaServicoInstance.icone || !iconeCategoriaServicoInstance.iconeType) {
			response.sendError(404)
			return
		}
		response.contentType = iconeCategoriaServicoInstance.iconeType
		response.contentLength = iconeCategoriaServicoInstance.icone.size()
		OutputStream out = response.outputStream
		out.write(iconeCategoriaServicoInstance.icone)
		out.close()
	}
	
	def update(){
		CategoriaServico categoriaServicoInstance = CategoriaServico.get(params.id)
		
		categoriaServicoInstance.nome = params.nome
		
		CategoriaServico aux = new CategoriaServico(params)
		
		categoriaServicoInstance.quesitos = aux.quesitos 		
			
		categoriaServicoInstance.save(flush: true)
		
		try {
			// Get the avatar file from the multi-part request
			def f = request.getFile('icone')
			
			if (!f.empty) {
				// List of OK mime-types
				if (!okcontents.contains(f.getContentType())) {
					flash.message = "O &iacute;cone deve ser: ${okcontents}"
					redirect(action:params.view, id: params.id)
					return
				}
			
				// Save the image and mime type
				categoriaServicoInstance.icone = f.bytes
				categoriaServicoInstance.iconeType = f.contentType
				log.info("File uploaded: $categoriaServicoInstance.iconeType")
						
				// Validation works, will check if the image is too big
				if (!categoriaServicoInstance.save()) {
					redirect(action:params.view, id: params.id)
					return
				}
			}
		}
		catch (Exception e) {
			flash.message = "&Iacute;cone (${categoriaServicoInstance.iconeType}, ${categoriaServicoInstance.icone.size()} bytes) n�o enviado."
			redirect(action:params.view, id: params.id)
		}
		
		redirect(action:show, id: params.id)
	}
	
	def list(Integer max) {
		params.max = Math.min(max ?: 10, 100)
				
		def criteria = CategoriaServico.createCriteria()
		def categoriaServicoInstanceList = criteria.list(params) {
			order('nome', 'asc')			
		}	
		
		[categoriaServicoInstanceList: categoriaServicoInstanceList, categoriaServicoInstanceTotal: categoriaServicoInstanceList.totalCount]
	}
}
