package br.com.servicosonline

import org.compass.core.engine.SearchEngineQueryParseException
import org.springframework.security.access.annotation.Secured
import org.springframework.security.core.authority.GrantedAuthorityImpl
import org.springframework.security.core.context.SecurityContextHolder

class FavoritoController {
    static scaffold = true
	
	def beforeInterceptor = [action:this.&auth] //, except:["index", "list", "show"]]
	
	def auth() {
		if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new GrantedAuthorityImpl('ROLE_ANONYMOUS'))){
			redirect(uri: "/login/auth")
			return false
		}
	}
	
	@Secured(['ROLE_ADMIN'])
	def create (){
		
	}
	
	@Secured(['ROLE_ADMIN'])
	def edit (){
		
	}
	
	static String WILDCARD = "*"
	def searchableService
	
	/**
	 * Index page with search form and results
	 */
	def search = {
		if (!params.q?.trim()) {
			return [:]
		}
		try {
			String searchTerm = WILDCARD + params.q + WILDCARD
			return [searchResult: Favorito.search(searchTerm, params)]
			//return [searchResult: searchableService.search(params.q, params)]
		} catch (SearchEngineQueryParseException ex) {
			return [parseException: true]
		}
	}

	/**
	 * Perform a bulk index of every searchable object in the database
	 */
	def indexAll = {
		Thread.start {
			searchableService.index()
		}
		render("bulk index started in a background thread")
	}

	/**
	 * Perform a bulk index of every searchable object in the database
	 */
	def unindexAll = {
		searchableService.unindex()
		render("unindexAll done")
	}
	
	def list(Integer max) {
		params.max = Math.min(max ?: 10, 100)
				
		def criteria = Favorito.createCriteria()
		def favoritoInstanceList = criteria.list(params) {
			if(params.byuser == '1'){
				Usuario usuario = Usuario.get(SecurityContextHolder.getContext().getAuthentication().getPrincipal().id)
				if(usuario){
					eq("usuario", usuario)
				}
			}			
		}
		
		[favoritoInstanceList: favoritoInstanceList, favoritoInstanceTotal: favoritoInstanceList.totalCount]
	}
	
}
